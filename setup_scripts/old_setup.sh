#!/bin/bash
# init
export GITS=~

function pause(){
   read -p "$*(enter to continue)"
}
echo "============================================="
echo  "This script is designed to install OpenCo's eCommerce platform on a debian or Ubuntu based system."
echo  "Periodically it will tell you what it's about to do. Sometimes you will be required to do things."
pause "About to apt-get git and mysql and other stuff (look at the script if you want to see)."
sudo apt-get -y  update
echo "============================================="
echo "What is your mysql root password going to be?"
pause "Remember it, you're going use it later."

sudo debconf-set-selections <<< 'mysql-server-<version> mysql-server/root_password Ball+Gain+Size+384'
sudo debconf-set-selections <<< 'mysql-server-<version> mysql-server/root_password_again password Ball+Gain+Size+384'

sudo apt-get -y install mysql-client-core-5.5 mysql-server
echo "============================================="
echo "Setting permissions on database."
echo "You will have to enter the mysql root password."
mysql -u root -p -e "create database store; GRANT ALL PRIVILEGES ON store.* To 'store'@'localhost' IDENTIFIED BY 'Fail+Type+Rose+555'"
echo "============================================="
echo "Installing ubuntu development packages. (apt-get)"
sudo apt-get -y install build-essential libssl-dev python-dev cmake
#sudo apt-get install libjpeg62 libjpeg62-dev
sudo apt-get -y install zlib1g-dev libfreetype6 libfreetype6-dev
sudo apt-get -y install nginx php5-cli php5-cgi spawn-fcgi psmisc php5-gd
sudo apt-get -y install python-setuptools
sudo apt-get -y install python-virtualenv
sudo apt-get -y install python-pip
sudo apt-get -y install git git-flow vim libmysqlclient-dev subversion
sudo apt-get -y install libjpeg-dev
#Problem !!!! ^this^ doesn't properly get used by pil
sudo apt-get -y install libyaml-dev

echo "Installing uwsgi with pip"
sudo pip install uwsgi
mkdir $GITS/uwsgi-logs 
echo "============================================="
echo "Installing virtualenv and virtualenvwrapper. (pip)"
sudo pip install virtualenvwrapper
echo "Setting up virtual env..."
export PIP_RESPECT_VIRTUALENV=true
export WORKON_HOME=~/Envs
mkdir -p $WORKON_HOME
source /usr/local/bin/virtualenvwrapper.sh
mkvirtualenv --no-site-packages env

echo "============================================="
echo "The following should be added to your .bashrc file."
echo "============================================="
echo "export PIP_RESPECT_VIRTUALENV=true"
echo "export WORKON_HOME=~/Envs"
echo "mkdir -p \$WORKON_HOME"
echo "source /usr/local/bin/virtualenvwrapper.sh"
echo "source $WORKON_HOME/env/bin/activate"
echo "============================================="
pause 

echo "Activating virtual env..."
source $WORKON_HOME/env/bin/activate

echo "Installing pip packages in virtual env..."
pip install django
pip install sorl-thumbnail==3.2.5
pip install pycrypto
pip install http://www.satchmoproject.com/snapshots/trml2pdf-1.2.tar.gz
pip install django-registration
pip install reportlab
pip install pyyaml
pip install PIL
pip install virtualenvwrapper

echo "========================================================="
echo "Generating ssh key..."
ssh-keygen
echo "========================================================="
cat  /home/livelaug/.ssh/id_rsa.pub
echo "========================================================="
pause "That's your ssh rsa pub key. Save it where it's needed."
echo  "pulling a git repo of the project here to $GITS"
cd $GITS
git  clone git@bitbucket.org:openco/laughing-man.git
cd $GITS/laughing-man/

#git config --global user.email ""
#git config --global user.name "openco"
sudo mkdir /etc/uwsgi
sudo mkdir /etc/uwsgi/vassals-enabled
sudo ln -s ~/laughing-man/uwsgi_app1.ini /etc/uwsgi/vassals-enabled/livelaugh_uwsgi_app1.ini
sudo ln -s ~/laughing-man/src/init.d/uwsgi /etc/init.d/uwsgi

ln -s ~/laughing-man/store/settings.py.dev ~/laughing-man/store/settings.py
ln -s ~/laughing-man/store/local_settings.py.dev  ~/laughing-man/store/local_settings.py
echo "Installing project requierments"
pip install -r requirements.txt
# The following isn't quite right yet... deals with installing the stuff we habe in our repo only...
add2virtualenv ~/laughing-man/lib/python2.7/site-packages/Satchmo-0.9.3-py2.7.egg
add2virtualenv ~/laughing-man/store #shouldn't have to do this ....
pip install django-grappelli
pip install uwsgi
pip install distribute --upgrade
pip install MySQL-python

# fix problem in helpdesk by overiting it's urls with ours... I'm thinking we should maybe just symlink things in there rather than doing the installs. Will test later need this running ASAP.
cp ~/laughing-man/lib/python2.7/site-packages/helpdesk/urls.py ~/Envs/env/lib/python2.7/site-packages/helpdesk/urls.py -f
# setup tclink
cd ~/laughing-man/src/tclink-3.4.4-python
python setup.py install

#echo "Enableing our site nginx.conf by linking it into the sites-enabled dir"
#sudo ln  ~/laughing-man/store_nginx.conf /etc/nginx/sites-enabled/
echo "Also we should remove the default nginx conf"
sudo rm /etc/nginx/sites-enabled/default

mkdir $GITS/database_backups/
ln -s $GITS/laughing-man/src/dbsync/grab.sh $GITS/database_backups/
ln -s $GITS/laughing-man/src/dbsync/backup.sh $GITS/database_backups/
ln -s $GITS/laughing-man/src/dbsync/restore.sh $GITS/database_backups/

echo "============================================="
echo "grabbing e9's design stuff"
cd ~
git clone git@bitbucket.org:openco/e9-laughing-man-content.git
ln -s ~/e9-laughing-man-content ~/laughing-man/store/static/e9

echo "============================================="
echo "Going to go grab the production db and sync it into our local db"
pause "You need to have your ssh key on the server you are grabbing the database from." 

cd $GITS/database_backups/
source grab.sh
cd ~
echo "grabbing ssl keys"
rsync -avhzP  --stats  secure.laughingmancheckout.com:~/ssl ~/
echo "I really recomend using Vundle and YouCompleateMe with vim"
echo "git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle"
echo "ln -s ~/laughing-man/src/livelaug/vimrc ~/.vimrc"
echo "============================================"
echo "Now we should have everthing lets start up the servers"
mkdir -p ~/public_html
ln -s /home/livelaug/laughing-man/store/media ~/public_html/media
ln -s /home/livelaug/laughing-man/store/static-collect ~/public_html/static
sudo ln -s ~/laughing-man/php-fastcgi /usr/bin/php-fastcgi
sudo chmod +x /usr/bin/php-fastcgi
sudo mkdir /var/run/php-fastcgi
sudo ln -s ~/laughing-man/store_nginx.conf /etc/nginx/sites-enabled/livelaug.conf
sudo /etc/init.d/nginx restart
pause "Once you have your db run: uwsgi --socket :8001 --wsgi-file uwsgi_app.py"

