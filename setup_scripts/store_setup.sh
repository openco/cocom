#!/bin/bash
patch /home/docker/cocom_site/cocom_site/settings.py < /home/docker/setup_scripts/settings.py.round2.patch
python /home/docker/cocom_site/manage.py makemigrations
python /home/docker/cocom_site/manage.py migrate
python /home/docker/cocom_site/manage.py satchmo_load_l10n
python /home/docker/cocom_site/manage.py runserver 0.0.0.0:80
