#!/bin/bash
python manage.py createsuperuser --username admin --email admin@openco.ca --noinput
python manage.py shell <<EOF
from django.contrib.auth.models import User
u = User.objects.get(username__exact="admin")
u.set_password("admin")
u.save()
EOF
