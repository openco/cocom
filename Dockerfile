# Copyright 2014 Alexander Somma, Andrew Jones
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific langage governing permissions and
# limitations under the License.

FROM ubuntu:trusty

maintainer OpenCo

#RUN echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get autoclean
RUN apt-get clean
RUN apt-get autoremove
RUN apt-get upgrade -y
RUN apt-get --fix-missing install -y build-essential
RUN apt-get install dpkg-dev
RUN apt-get install -y apt-utils
RUN apt-get install -y build-essential
RUN apt-get install -y libssl-dev python-dev cmake
RUN apt-get install -y zlib1g-dev libfreetype6 libfreetype6-dev
RUN apt-get install -y python python-dev python-setuptools python-virtualenv
RUN apt-get install -y python-pip
RUN apt-get install -y git git-flow libmysqlclient-dev subversion
RUN apt-get install -y libjpeg-dev
RUN apt-get install -y libyaml-dev
RUN apt-get install -y mercurial csstidy
RUN apt-get install -y nginx supervisor
RUN apt-get install -y python-memcache
RUN apt-get install -y vim

# Install mysql and make sure it doesn't bother us with questions
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-client mysql-server
RUN apt-get install pwgen

RUN apt-get install -y python-software-properties software-properties-common
RUN apt-get install -y sqlite3

# install uwsgi first because it takes a little while
RUN pip install uwsgi

# install ipython for better debugging ... might want to take this out or build an image against this image as a debugging image
RUN pip install ipython==5.6

#
# Install General Python Packages
#
RUN pip install MySQL-python

#
# Install django
#
RUN pip install django==1.7.11
#RUN pip install south
RUN pip install django-filer==0.9.12
RUN pip install django-polymorphic==0.7.2 # Installed as a requirement of django-filer
RUN pip install easy-thumbnails==2.2.1 # Installed as a requirement of django-filter
RUN pip install Pillow==3.0.0         # Installed as a requirement of easy-thumbnails
RUN pip install django-registration-redux==1.2

#
# Install Django Extras
#
RUN pip install django-grappelli==2.7.2
RUN pip install django-suit==0.2.15
RUN pip install django-debug-toolbar==1.3.2
#RUN pip install django-debug-toolbar-autoreload==0.2.0
# Dajax is a powerful tool to easily and super-quickly develop #
# asynchronous presentation logic in web applications          #
RUN pip install django-dajax==0.9.2
#RUN pip install django-dajaxice==0.5.5  # Installed as a requirement of django-dajax

#
# Install common requirements for Djangocms and Satchmo
#
RUN pip install distribute     # This seems to be intalled already by this point 

#
# Install Djangocms 
#
RUN pip install argparse==1.2.1
RUN pip install django-cms==3.0.16
#RUN pip install django-classy-tags==0.4  # Installed as a requirement of django-cms
RUN pip install django-mptt==0.7.4              # Installed as a requirement of django-cms
#RUN pip install django-sekizai==0.7      # Installed as a requirement of django-cms
#RUN pip install html5lib==1.0b3          # Installed as a requirement of django-cms
RUN pip install six==1.5.2       # TODO It's already installed for some reason.  Why?
RUN pip install wsgiref==0.1.2   # TODO It's already installed for some reason.  Why?
RUN pip install cmsplugin-filer==0.10.2
RUN pip install django-smart-load-tag==0.3.2
RUN pip install django-admin-bootstrapped==0.3.2
RUN pip install django-compressor==1.6 # Installed as a requirement of coffee-compressor-compiler
RUN pip install coffee-compressor-compiler==0.2.0

# Djangocms plugins
RUN pip install djangocms-text-ckeditor==2.6.0
RUN pip install djangocms-flash==0.2.0
RUN pip install djangocms-googlemap==0.3
RUN pip install djangocms-link==1.7.1
RUN pip install djangocms-snippet==1.5

#
# Djangocms Extras
#
RUN pip install cmsplugin-redirect==0.2.1
RUN pip install django-reversion==1.6
# Django Form Designer is an o app for building many kinds of  #
# forms visually, without any programming knowledge.           #
# For now we have to install from it's git repo because Pypi's #
# version is broken                                            #
# Changed git url to a fork that should work with 1.7 -fx      #
RUN pip install git+git://github.com/andersinno/django-form-designer.git#egg=django-form-designer

#
# Install Satchmo 
#
RUN pip install pytz
RUN pip install pycrypto
RUN pip install https://web.archive.org/web/20120120040420/http://www.satchmoproject.com/snapshots/trml2pdf-1.2.tar.gz
RUN pip install PyYAML
RUN pip install reportlab
RUN pip install sorl-thumbnail==12.3
RUN pip install django-livesettings
RUN pip install django-threaded-multihost
RUN pip install django-caching-app-plugins
RUN pip install django-signals-ahoy
RUN pip install django-keyedcache
RUN pip install hg+http://bitbucket.org/chris1610/satchmo/#egg=satchmo


RUN pip install django-picklefield  # What's this?

#
# Django-bower package managment of front end libraries        #
#
RUN pip install django-bower==4.8.1

################################################################
# Django-reportengine siplifies report generation              #
################################################################
#RUN pip install django-reportengine==0.3.1  # Not compatible with django 1.6 urls.py

################################################################
# cmsplugin-feedparser                                         #
# TODO fork and tag cmsplugin-feedparser releases              #
################################################################
RUN pip install django-feedparser>=0.2.0
# sveetch/cmsplugin-feedparser too unstable forking
# in cocom's plugns

################################################################
# Django REST 
################################################################
RUN pip install djangorestframework==2.4.4
RUN pip install markdown  # Markdown support for the browsable API.
RUN pip install django-filter==0.9.2  # Filtering support
RUN pip install -e git://github.com/bkeating/python-payflowpro.git#egg=python_payflowpro


################################################################
# Twitter and postage requirments                              #
################################################################

RUN pip install suds
RUN pip install twill
RUN pip install twython
RUN pip install django-sslify

# Breaking this up for faster rebuilds
## install our code
#ADD . /home/docker/
#RUN mv /home/docker/start.sh /start.sh
WORKDIR /home/docker/cocom_site


# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default
ADD nginx-app.conf /home/docker/
ADD supervisor-app.conf /home/docker/
ADD uwsgi_params /home/docker/
RUN ln -s /home/docker/nginx-app.conf /etc/nginx/sites-enabled/
RUN ln -s /home/docker/supervisor-app.conf /etc/supervisor/conf.d/
RUN mkdir /var/log/uwsgi

#Patches ... 
ADD setup_scripts /home/docker/setup_scripts
RUN patch  /usr/local/lib/python2.7/dist-packages/product/views/__init__.py < /home/docker/setup_scripts/product_views___init__.patch

##Patching django app registry
##This is a stand in until thumbnail apps upgraded to use registry
#RUN patch /usr/local/lib/python2.7/dist-packages/django/apps/registry.py < /home/docker/setup_scripts/registry.patch

# Instead patching sorl
RUN patch  /usr/local/lib/python2.7/dist-packages/sorl/thumbnail/__init__.py < /home/docker/setup_scripts/sorl.patch

# Patching livesettings
#RUN patch /usr/local/lib/python2.7/dist-packages/livesettings/values.py < /home/docker/setup_scripts/livesettings.patch 
#RUN patch /usr/local/lib/python2.7/dist-packages/livesettings/templates/livesettings/site_settings.html < /home/docker/setup_scripts/site_settings.patch

# Patch mail to include proper context
RUN patch /usr/local/lib/python2.7/dist-packages/satchmo_store/mail.py < /home/docker/setup_scripts/mail.patch

# Patch feedparser to not fail on SSL errors
# TODO Fix feedparser SSL
RUN patch /usr/local/lib/python2.7/dist-packages/django_feedparser/renderer.py < /home/docker/setup_scripts/django_feedparser_renderer.patch


# Patch payment form to include proper context
RUN patch /usr/local/lib/python2.7/dist-packages/payment/forms.py  < /home/docker/setup_scripts/payment_forms.py.patch

# Patch shipping to includ proper context
RUN patch /usr/local/lib/python2.7/dist-packages/shipping/views.py < /home/docker/setup_scripts/shipping_views.py.patch

# TODO make patch
ADD setup_scripts/satchmo_category.py /usr/local/lib/python2.7/dist-packages/satchmo_store/shop/templatetags/satchmo_category.py

#Comment out to get to the shell while there is a label conflict
#RUN python /home/docker/cocom_site/manage.py syncdb --noinput

ADD cocom_site /home/docker/cocom_site
RUN python /home/docker/cocom_site/manage.py makemigrations
RUN python /home/docker/cocom_site/manage.py migrate
RUN /home/docker/setup_scripts/create_superuser.sh
#RUN python /home/docker/cocom_site/manage.py collectstatic --noinput

ADD include /home/docker/include
RUN cp /home/docker/include/*.png /usr/local/lib/python2.7/dist-packages/cms/static/cms/img/toolbar/
RUN cp /home/docker/include/welcome.html /usr/local/lib/python2.7/dist-packages/cms/templates/cms/welcome.html

RUN mkdir /home/docker/cocom_site/cocom_site/static/dajaxice/
RUN cp /usr/local/lib/python2.7/dist-packages/dajaxice/templates/dajaxice/dajaxice.core.js /home/docker/cocom_site/cocom_site/static/dajaxice

# Patch the settings
# TODO segrigate round2 stuff
RUN patch /home/docker/cocom_site/cocom_site/settings.py < /home/docker/setup_scripts/settings.py.round2.patch
RUN python /home/docker/cocom_site/manage.py makemigrations
RUN python /home/docker/cocom_site/manage.py migrate
RUN python /home/docker/cocom_site/manage.py loaddata /usr/local/lib/python2.7/dist-packages/satchmo_store/contact/fixtures/initial_data.yaml

ADD static /home/docker/cocom_site/cocom_site/static
ADD templates /home/docker/cocom_site/templates

ADD static/admin /usr/local/lib/python2.7/dist-packages/django/contrib/admin/static/admin

RUN mkdir -p /home/docker/cocom_site/static
RUN python /home/docker/cocom_site/manage.py collectstatic --noinput -c

# Adding SSL certs
RUN mkdir -p /etc/ssl/certs/
RUN mkdir -p /etc/ssl/private/
ADD ssl/myssl.crt /etc/ssl/certs/myssl.crt
ADD ssl/myssl.key /etc/ssl/private/myssl.key

ADD start.sh /home/docker/start.sh
#expose 80
expose 443
cmd ["bash", "/home/docker/start.sh"]
