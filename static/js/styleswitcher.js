var activeStylesheet = (function() {

  // Store title of default linked stylesheet
  var defaultTitle;

  return {

    // Set the active stylesheet to the link styesheet element with title
    // Set all others to disabled
    setActive: function(title) {
      var link, links = document.getElementsByTagName('link');

      for (var i=0, iLen=links.length; i<iLen; i++) {
        link = links[i];

        if (link.rel.indexOf("style") != -1 && link.title) { 
          link.disabled = true; 

          if (link.title == title) {
            link.disabled = false; 
          }
        }
      }
    },

    // Return the title of the currently active linked stylesheet,
    // or undefined if none found
    getActive: function() {
      var link, links = document.getElementsByTagName('link');

      for (var i=0, iLen=links.length; i<iLen; i++) {
        link = links[i];

        if (link.rel.indexOf("style") != -1 &&
            link.title &&
           !link.disabled) {
          return link.title;
        }
      }
    },

    // Return the title of the link stylesheet element where 
    // rel property contains 'alt'
    getPreferred: function() {
      var link, links = document.getElementsByTagName('link');

      for (var i=0, iLen=links.length; i<iLen; i++) {
        link = links[i];

        if (link.rel.indexOf("style") != -1 && 
            link.rel.indexOf('alt') == -1 &&
            link.title) { 
          defaultTitle = title;
          return link.title; 
        }
      }
    }
  };
}());
