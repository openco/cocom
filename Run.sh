#!/bin/bash
# OpenCo Cocom Build 'n' Run Script 
# Version 1.3.5

LIVESERVER=openco

PROJECTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    PROJECT=${PROJECTPATH##*/}
FULL_BRANCH=$(git rev-parse --abbrev-ref HEAD)
# Lowecase the branch
FULL_BRANCH=`echo $FULL_BRANCH|awk '{print tolower($0)}'`
     BRANCH=${FULL_BRANCH##*/}
        TAG=$(git describe --always --tag)

  COCOMSITE=/home/docker/cocom_site

if [ -e /c/Windows/win.ini ]
  then WINPTY="winpty"
fi

if [ $HOSTNAME != $LIVESERVER ]
  then
    echo "This is not the Production Server"
    #PORTS="-p 80:80 -p 443:443"
    PORTS="-p 443:443"
    VHOST="cocom-dev"
  else
    echo "This is The Production Server"
    #PORTS="-p 80 -p 443"
    PORTS="-p 443"
    VHOST="cocom"
fi

echo "PROJECTPATH="$PROJECTPATH
echo "PROJECT="$PROJECT
echo "BRANCH="$BRANCH

echo "$WINPTY docker run -i -t $PORTS \
  --rm \
  --name cocom-dev \
  -v $PROJECTPATH/templates:/home/docker/cocom_site/templates \
  -v $PROJECTPATH/static:/home/docker/cocom_site/cocom_site/static \
  -e VIRTUAL_HOST=cocom-dev \
  -e LANG=en_US.UTF-8 \
  openco/$PROJECT-$BRANCH:latest"

$WINPTY docker run -i -t $PORTS \
  --rm \
  --name cocom-dev \
  -v $PROJECTPATH/templates:$COCOMSITE/templates \
  -v $PROJECTPATH/static:$COCOMSITE/cocom_site/static \
  \
  -v $PROJECTPATH/cocom_site/scheduler:$COCOMSITE/scheduler \
  \
  -e VIRTUAL_PROTO=https \
  -e VIRTUAL_PORT=443 \
  -e CERT_NAME=myssl \
  -e VIRTUAL_HOST=$VHOST \
  -e LANG=en_US.UTF-8 \
  openco/$PROJECT-$BRANCH:latest

