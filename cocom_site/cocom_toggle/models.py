from django.db import models

#from cms.plugins.text.models import Text # Old text editor
from djangocms_text_ckeditor.models import AbstractText

class ToggleModel(AbstractText):

    title = models.CharField(max_length=255)

    COLOUR_CHOICES = (
         ('1', 'Colour 1'),
         ('2', 'Colour 2'),
        )
    colour = models.CharField(max_length=5, choices=COLOUR_CHOICES, default='1')

    open = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title
    
