from cms.plugin_pool import plugin_pool
from models import ToggleModel
from cocom_site.settings import STATIC_URL
#from cms.plugins.text.cms_plugins import TextPlugin
from djangocms_text_ckeditor.cms_plugins import TextPlugin
from django.utils.translation import ugettext_lazy as _

class TogglePlugin(TextPlugin):
    model = ToggleModel
    name = _("Toggle")
    render_template = "toggle.html"

    def render(self, context, instance, placeholder):
        context['instance'] = instance
        return super(TogglePlugin, self).render(context, instance, placeholder)

plugin_pool.register_plugin(TogglePlugin)
