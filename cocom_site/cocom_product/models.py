from django.db import models
from product.models import Product, ProductManager, Category
from cms.models.fields import PlaceholderField
from cms.models.pagemodel import Page

class CocomProduct(Product):
    """
    Custom Product for events.
    """ 
    objects = ProductManager()
    placeholder_1 = PlaceholderField('placeholder_1', related_name='product_ph_1_set')
    placeholder_2 = PlaceholderField('placeholder_2', related_name='product_ph_2_set')

    def __unicode__(self):
        return u'%s' % (self.name)

class CocomCategory(Category):

    placeholder_1 = PlaceholderField('placeholder_1', related_name='category_ph_1_set')
    placeholder_2 = PlaceholderField('placeholder_2', related_name='category_ph_2_set')

    def __unicode__(self):
        return self.name

#class CocomPageCategory(Page, Category):
#
#    def __unicode__(self):
#        return self.name
