# Main Django admin
from django.contrib import admin

# Django CMS admin
from cms.admin.placeholderadmin import PlaceholderAdmin
from cms.admin.pageadmin import PageAdmin

# The Satchmo product admin
from product.admin import ProductOptions, CategoryOptions

# The model
from models import CocomProduct, CocomCategory
#CocomPageCategory

class CocomProductAdmin(PlaceholderAdmin, ProductOptions):

    # 1) ProductOptions defines a fieldset list
    # 2) PlaceHolderAdmin overrides the method get_fieldset from the django.contrib.admin
    # 3) But PaceHolderAdmin.get_fieldset() uses the preexisting fieldset list if it finds one.
    #    This is so you can override it's functionality.  But we want it's functionality!
    # 4) That means we need to clear the fieldset list that ProductOptions made during our initalization.
    # 5) Python requires explicit calls to supercontructors from the contructor.
    def __init__(self, var1, var2):
        # The satchmo super-contructor
        ProductOptions.__init__(self, var1, var2)

        # Clear the satchomo fieldset
        self.fieldsets = None

        # The django-cms super-contructor
        PlaceholderAdmin.__init__(self, var1, var2)


    # We override the get_fieldset() method from PlaceholderAdmin in case we want to filter its result.
    def get_fieldsets(self, request, obj=None):

        # Get the fieldset that PlaceholderaAdmin generates
        fs = PlaceholderAdmin.get_fieldsets(self, request, obj)

        # TODO:
        # Mess with the fieldset here.

        return fs

class CocomCategoryAdmin(PlaceholderAdmin, CategoryOptions):
    # We don't have to do anything.  
    # We acheived everything we wanted from the multiple-inheritance.
    pass

admin.site.register(CocomProduct, CocomProductAdmin)
admin.site.register(CocomCategory, CocomCategoryAdmin)
