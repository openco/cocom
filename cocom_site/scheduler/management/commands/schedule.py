from django.core.management.base import BaseCommand, CommandError
from scheduler.stamps_lib import StampsLib, Constraints
import scheduler.postage_settings
import scheduler.openco_scheduler

class Command(BaseCommand):
    args = '<nothing at the moment>'
    help = 'Tests StampsLib status.'

    def handle(self, *args, **options):
        scheduler.openco_scheduler.scheduler()
