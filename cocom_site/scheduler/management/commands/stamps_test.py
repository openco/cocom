from django.core.management.base import BaseCommand, CommandError
from scheduler.stamps_lib import StampsLib, Constraints
import scheduler.postage_settings

##############################################################################
#
# Test functions
#
##############################################################################

def test_wsdl(stampsLib):
# output the WSDL parse from suds
    stampsLib.output_wsdl()

def test_login(stampsLib):
    success = stampsLib.login()
    if (success):
        print "Success"
    else:
        print "Failed"

def test_purchase_postage(stampsLib):
    success = stampsLib.login()
    if (not success):
        return
    success = stampsLib.purchase_postage_transaction(100);


def test_create_indicum(stampsLib):
    success = stampsLib.login()
    if (not success):
        return
#    success = stampsLib.create_indicium_transaction(
#        '13',                   # transactionID.
#        'Jame Spelman',         # name
#        '620 S. Burchard Ave',  # address1
#        '',                     # address2
#        'Freeport',             # city
#        'IL',                   # state
#        '61032',                # zip
#        '',                     # phone
#        1,                    # weight in pounds
#        70                   # volume in cubic inches
#        )

    success = stampsLib.create_indicium_transaction(
        '14',                   # transactionID.
        'Jame Spelman',         # name
        '183 DUANE STREET',  # address1
        '',                     # address2
        'NEW YORK',             # city
        'NY',                   # state
        '10013',                # zip
        '',                     # phone
        4,                    # weight in pounds
        100                   # volume in cubic inches
        )



def test_boxes(stampsLib):

    print "Testing Coffees..."

    # Coffee
    coffee = Constraints()
    coffee.weight = 1
    coffee.volume = 1
    coffee.dim1 = 1
    coffee.dim2 = 1
    coffee.dim3 = 1

    test_item_factors(stampsLib, coffee)

def test_item_factors(stampsLib, item):
    for i in range(1,12):
        print str(i)
        product = multiply(item, i)
        stampsLib.test_boxes(product)
        rate_flat = stampsLib.rateInfoFlat.rateInfoString()
        rate_reg = stampsLib.rateInfoReg.rateInfoString()
        rate_weight = stampsLib.rateInfoWeight.rateInfoString()
        print rate_flat
        print rate_reg
        print rate_weight

def multiply(constraints, factor):

    product = Constraints()
    product.weight = constraints.weight * factor
    product.volume = constraints.volume * factor
    product.dim1 = constraints.dim1
    product.dim2 = constraints.dim2
    product.dim3 = constraints.dim3

    return product

class Command(BaseCommand):
    args = '<nothing at the moment>'
    help = 'Tests StampsLib status.'

    def handle(self, *args, **options):
        # Initialize logging
        #formatter = logging.Formatter(fmt='%(asctime)s\t%(name)s:\t%(levelname)s:\t%(message)s', datefmt=None)
        #handler = logging.FileHandler('postage.log', mode='a', encoding=None, delay=False)
        #handler.setFormatter(  formatter )

        #logger = logging.getLogger('stamps_test')
        #logger.propagate = False
        #logger.setLevel(logging.INFO)
        #logger.addHandler( handler )

        # Initialize StampsLib
        stampsLib = StampsLib(None, False)

        #test_wsdl(stampsLib)
        test_login(stampsLib)
        #test_purchase_postage(stampsLib)
        #test_create_indicum(stampsLib)
        test_boxes(stampsLib)
