from django.core.management.base import BaseCommand, CommandError
from scheduler.openco_scheduler import init_logging
from scheduler.postage import *
from scheduler.stamps_lib import StampsLib, Constraints
from satchmo_store.shop.models import Order, OrderItem, Product, OrderVariable, OrderStatus

def test_boxes(stampsLib):

    # Coffee
    coffeeProduct = Product.objects.get(pk=1)
    print "Testing Coffees (" + str(coffeeProduct) + ") ..."
    coffeeConstraints = Constraints()
    volume = get_product_volume(coffeeProduct, coffeeConstraints)
    weight = get_product_weight(coffeeProduct)
    print "Volume: " + str(volume)
    print "Weight: " + str(weight)
    coffeeConstraints.volume = volume
    coffeeConstraints.weight = weight

    test_item_factors(stampsLib, coffeeConstraints)

    # Tea
    teaProduct = Product.objects.get(pk=2)
    print "Testing Teas (" + str(teaProduct) + ") ..."
    teaConstraints = Constraints()
    volume = get_product_volume(teaProduct, teaConstraints)
    weight = get_product_weight(teaProduct)
    print "Volume: " + str(volume)
    print "Weight: " + str(weight)
    teaConstraints.volume = volume
    teaConstraints.weight = weight

    test_item_factors(stampsLib, teaConstraints)

def test_item_factors(stampsLib, item):
    for i in range(1,13):
        print str(i)
        product = multiply(item, i)
        order_volume = round(product.volume,2)
        print "\tVolume: " + str(order_volume)
        print "\tWeight: " + str(round(product.weight,2))
        stampsLib.test_boxes(product)
        rate_flat = stampsLib.rateInfoFlat.rateInfoString()
        rate_reg = stampsLib.rateInfoReg.rateInfoString()
        rate_weight = stampsLib.rateInfoWeight.rateInfoString()
        volume_flat = get_volume(stampsLib.rateInfoFlat.box)
        volume_reg = get_volume(stampsLib.rateInfoReg.box)
        volume_weight = get_volume(stampsLib.rateInfoWeight.box)

        print "\t" + rate_flat + " (" + str(volume_flat) + ") " + str(order_volume - volume_flat)
        print "\t" + rate_reg + " (" + str(volume_reg) + ") " + str(order_volume - volume_reg)
        print "\t" + rate_weight + " (" + str(volume_weight) + " )" + str(order_volume - volume_weight)

def get_volume(box):
    if box == None:
        return 0
    return float(box.volume)

def multiply(constraints, factor):

    product = Constraints()
    product.weight = constraints.weight * factor
    product.volume = constraints.volume * factor
    product.dim1 = constraints.dim1
    product.dim2 = constraints.dim2
    product.dim3 = constraints.dim3

    return product

class Command(BaseCommand):
    args = '<nothing at the moment>'
    help = 'Tests postage'

    def handle(self, *args, **options):

	# Init Logging
	init_logging()

	# Initialize StampsLib
	stampsLib = StampsLib(None, False)

	test_boxes(stampsLib)

