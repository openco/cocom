#!/usr/bin/python
import logging
import time

import openco_scheduler_settings
import scheduler.postage as postage

# For stack trace.
import sys
import traceback

# For calling shell
from subprocess import call

def init_logging():
    global logger
    global handler

    # Initialize logging
    formatter = logging.Formatter(fmt='%(asctime)s\t%(name)s:\t%(levelname)s:\t%(message)s', datefmt=None)
    handler = logging.FileHandler('openco_scheduler.log', mode='a', encoding=None, delay=False)
    handler.setFormatter(  formatter )
    logger = logging.getLogger('openco_scheduler')
    logger.propagate = False
    logger.setLevel(logging.INFO)
    logger.addHandler( handler )

def log(message):
    global logger
    global handler

    logger.info(str(message));
    if not handler:
        print 'postage: ' + str(message)

def scheduler():
    global sys_info
    global handler

    init_logging()

    print "Scheduler Running. See openco_scheduler.log for details."
    log("Starting up...")

    # Initialize postage. We send it our log handler
    # The second is for logging to screen
    postage.init_postage(handler)

    # Detect CTLR-C
    try:

        # Loop forever
        while(True):

            if openco_scheduler_settings.postageEnabled:
                postage_event()

            if openco_scheduler_settings.twitterEnabled:
                twitter_event()

            # wait a bit
            time.sleep(openco_scheduler_settings.sleepSeconds)

    # Detect CTLR-C
    except KeyboardInterrupt as e:
        print "\nDetected KeyboardInterupt."
        log("Detected KeyboardInterupt.")

    print "Shutting down..."
    log("Shutting down...")


def postage_event():
    try:
        # processes orders
        postage.createIndicia()
    except Exception as e:
        log("Top level exception handler (postage): " + str(e))
        stacktrace()

def twitter_event():
    try:
        # call shell script
        call([openco_scheduler_settings.twitterScript, ""])
    except Exception as e:
        log("Top level exception handler (twitter): " + str(e))
        stacktrace()

def stacktrace():
    sys_info = sys.exc_info()
    traceback.print_tb(sys_info[2])


# If this file is being executed directly, start the postage daemon
if __name__ == "__main__":
    scheduler()
