##############################################################################
#
# Postage Deamon Settings
#    these are imported from postage_deamon.py
#
##############################################################################

# limit the number of orders handled per run
# to save memory
limit = 100

# Wait this number of seconds between database checks
sleepSeconds = 30

# This is the volume of the largest box that will be used.
# Larger volume orders will not be handled.
#
# "Black gift box large" is 15 1/4  x  10 1/4  x  6 1/2
# 15.25 * 10.25* 6.5 = 1016.03125 cubic inches
# Newer largest box:
# 17.25 * 11.5 * 12= 2380.5
# Newer largest box:
# 18 x 12 x 12 Volume: 2592
volumeCutOff = 2592

# The software looks for and handles orders with this shipping method.
shippingMethod = "US Mail"

# This is the thickness added to each dimension because of packaging.  
# It's multiplied by two since each dimension is affected twice by the 
# packaging.
# When this was .2, it caused 6 coffees to go into Large Flat Rate instead of Flat Rate
packingThickness = 0

# How much can things be squished?
teaSquishFactor = 1.25
coffeeSquishFactor = 0.97

