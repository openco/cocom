##############################################################################
#
# Stamps.com Library Settings
#    these are imported from stamps_lib.py
#
##############################################################################

# The integration account at stamps.com
integrationID = '1191d758-151a-4eef-82c8-842ea185f2e1'

# User account at stamps.com
# (OpenCo account on the testing server)
username = 'openco-1'
password = 'postage1'

# Location of the stamps.com WSDL
wsdl = 'file:///home/docker/cocom_site/scheduler/stamps_lib_v24_mod013_testing.wsdl'

# The location that the postage files are saved
postageDir = '/home/docker/cocom_site/scheduler/postage'

# Shipping From Address
# Laughing Man Coffee And Tea
# 184 Duane Street
# New York, NY 10013-3343
fromName = 'LAUGHING MAN COFFEE AND TEA'
fromAddress1 = '184 DUANE STREET'
fromCity = 'NEW YORK'
fromState = 'NY'
fromZIP = '10013'
fromPhoneNumber = '+1-212-680-1111'

# This controls whether real postage is generated
# or fake (testing) postage
testing = True

# From Sheila:
# the boxes we currently have:
# 14x8x8 (common)
# 14x10x4 (common)
# 11.25x8.75x6 (4,2)
# 8x8x8 (7,6,5,4)
# 10x9x4 (common)
# 17.25x11.5x12 (10,8,6) (common)

