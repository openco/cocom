
from decimal import *

##########################################################################
#
# Some helper objects
#
##########################################################################

#
# Represents a shipping method
#
class Method:
    def __init__(self, name):
        self.name = name
        self.packages = []
    def __str__(self):
        return "Method: " + self.name
    def __repr__(self):
        return self.__str__()
    def add_package(self, package):
        (self.packages).append(package)

    #
    # Finds a box big enough for weight and volume.
    # returns None if no box is big enough
    #
    def get_fit(self, weight, volume, dim1, dim2, dim3):
        for package in self.packages:
            box = package.get_fit(weight, volume, dim1, dim2, dim3)
            if box != None:
                return box
        return None

#
# Represents a type of package.
# (a group of boxes)
#
class Package:
    def __init__(self, name):
        self.name = name
        self.boxes = []
        self.weight = Decimal(0)
        self.volume = Decimal(0)
        self.dim1 = Decimal(0)
        self.dim2 = Decimal(0)
        self.dim3 = Decimal(0)
    def __str__(self):
        return "Package: " + self.name.ljust(34) 
        # + " Volume:" + str(self.volume) + "  Max Weight:" + str(self.weight)
    def __repr__(self):
        return self.__str__()

    def add_box(self, box):
        # Link from package to box
        (self.boxes).append(box)

        # Link from box to package
        box.package = self

        # Increase package attribs based on box attribs.
        #if (self.volume < box.volume):
        #    self.volume = box.volume
        #if (self.weight < box.weight):
        #    self.weight = box.weight
        #if (self.dim1 < box.dim1):
        #    self.dim1 = box.dim1
        #if (self.dim2 < box.dim2):
        #    self.dim2 = box.dim2
        #if (self.dim3 < box.dim3):
        #    self.dim3 = box.dim3

    #
    # Finds a box big enough for weight and volume
    # returns None if no box is big enough
    #
    def get_fit(self, weight, volume, dim1, dim2, dim3):
        for box in self.boxes:
            if box.fits(weight, volume, dim1, dim2, dim3):
                return box
        return None

#
# Represents a box
#
class Box:
    def __init__(self, dim1, dim2, dim3, maxWeight, boxWeight, volume):

        # Primary attributes
        self.dim1 = Decimal(dim1)
        self.dim2 = Decimal(dim2)
        self.dim3 = Decimal(dim3)
        self.maxWeight = Decimal(maxWeight)
        self.boxWeight = Decimal(boxWeight)
        self.package = None
        self.volume = volume

        # Computed attributes
        self._calc_volume()
        self._calc_circ()
        self._calc_description()


    def __str__(self):
        return "Box: " + self.description.ljust(30) + self.attribs()
    def __repr__(self):
        return self.__str__()

    # Takes boxes dimensions in inches
    # Returns volume in cubic inches
    def _calc_volume(self):
        if self.volume == None:
            self.volume = self.dim1 * self.dim2 * self.dim3

    # This calculates a weighted circumference that USPS uses to catagorise packages:
    # "Package" (Longest side plus longest circumference less than 84 inches)
    # "Large Package" (Longest side plus longest circumference between 84 inches and 108)
    # "Oversized Package" (Longest side plus longest circumference between 108 inches and 130)
    def _calc_circ(self):
        self.circ = (self.dim1 * 3) + (self.dim2 * 2)
        # TODO: check if the circumference exceeds 84 or 108

    def _calc_description(self):
        self.description = str(self.dim1) + " x " + str(self.dim2) + " x " + str(self.dim3)

    def attribs(self):
        return (
            "Volume:" + str(round(self.volume,2))
            + "  Max Weight:" + str(round(self.maxWeight,2))
            + "  Circ:" + str(round(self.circ,2))
            + "  Box Weight:" + str(round(self.boxWeight,2)) 
            )

    def fits(self, weight, volume, dim1, dim2, dim3):

        # atempt to elliminate ourself
        if (weight + self.boxWeight > self.maxWeight):
            return False
        if (volume > self.volume):
            return False
        if (dim1 > self.dim1):
            return False
        if (dim2 > self.dim2):
            return False
        if (dim3 > self.dim3):
            return False

        # All checks passed
        return True


##########################################################################
#
# Main box database object
#
##########################################################################

class StampsBoxes:

    _methods = []

    def __init__(self):

        # Method Types
        method_flat_rate =   Method('Flat Rate')
        method_reg_rate =    Method('Regional Rate') 
        method_weight_rate = Method('By Weight Rate') 
        self.add_method( method_flat_rate )
        self.add_method( method_reg_rate )
        self.add_method( method_weight_rate )

        # Package Types
        package_flat_small = Package('Small Flat Rate Box')
        package_flat_med =   Package('Flat Rate Box')
        package_flat_large = Package('Large Flat Rate Box')
        method_flat_rate.add_package( package_flat_small )
        method_flat_rate.add_package( package_flat_med )
        method_flat_rate.add_package( package_flat_large )

        package_reg_a = Package('Regional Rate Box A')
        package_reg_b = Package('Regional Rate Box B')
        package_reg_c = Package('Regional Rate Box C')
        method_reg_rate.add_package( package_reg_a )
        method_reg_rate.add_package( package_reg_b )
        method_reg_rate.add_package( package_reg_c )

        package_weight_norm = Package('Package')
        package_weight_large = Package('Large Package')
        method_weight_rate.add_package( package_weight_norm )
        method_weight_rate.add_package( package_weight_large )

        #
        # Boxes
        #   IMPORTANT: Boxes must be added in increasing order of volume AND weight.
        #   IMPORTANT: Box dimensions must be specified in decreasing order.
        #
        package_flat_small.add_box( Box(8.625, 5.375, 1.625, 70, 0, None) )
        package_flat_med.add_box( Box(11, 8.5, 5.5, 70, 0, 314) ) 
        package_flat_med.add_box( Box(13.625, 11.875, 3.375, 70, 0, None) )
        #package_flat_large.add_box( Box(12, 12, 5.5, 70, 0, None) )
        #package_flat_large.add_box( Box(23.6875, 11.75, 3, 70, 0, None) )

        package_reg_a.add_box( Box(10, 7, 4.75, 15, 0.28, 285) )
        package_reg_a.add_box( Box(12.8125, 10.9375, 2.375, 15, 0.35, None) )
        package_reg_b.add_box( Box(12, 10.25, 5, 20, 0.48, None) )
        package_reg_b.add_box( Box(15.875, 14.375, 2.875, 20, 0.55, None) )
        #package_reg_c.add_box( Box(14.75, 11.75, 11.5, 25, 0, None) )

        package_weight_norm.add_box( Box(10, 9, 4, 70, 0.45, 300) )         # Good 
        package_weight_norm.add_box( Box(8, 8, 8, 70, 0.45, 370) )         # Good # 440
        package_weight_norm.add_box( Box(14, 10, 4, 70, 0.65, 548) )       # Good
        #package_weight_norm.add_box( Box(11.25, 8.75, 6, 70, 0, None) )    # Replaced
        package_weight_norm.add_box( Box(11, 9, 6, 70, 0.5, None) )         # Good
        #package_weight_norm.add_box( Box(14, 8, 8, 70, 0, None) )          # Removed
        #package_weight_norm.add_box( Box(17.25, 12, 11.5, 70, 0, None) )   # Replaced
        package_weight_norm.add_box( Box(18, 12, 6, 70, 1.4, None) )        # Multi-depth
        package_weight_norm.add_box( Box(17, 9, 9, 70, 0.8, None) )         # Good
        package_weight_norm.add_box( Box(18, 12, 8, 70, 1.4, None) )        # Multi-depth
        package_weight_norm.add_box( Box(18, 12, 10, 70, 1.4, None) )       # Multi-depth
        package_weight_norm.add_box( Box(18, 12, 12, 70, 1.4, None) )       # Multi-depth

        #packages['Letter'] = []
        #packages['Large Envelope or Flat'] = []
        #packages['Legal Flat Rate Envelope'] = []
        #packages['Thick Envelope'] = []
        #packages['Flat Rate Envelope'] = []
        #packages['Flat Rate Padded Envelope'] = []

        # From Sheila:
        # the boxes we currently have:
        # 14x8x8 (common)
        # 14x10x4 (common)
        # 11.25x8.75x6 (4,2)
        # 8x8x8 (7,6,5,4)
        # 10x9x4 (common)
        # 17.25x11.5x12 (10,8,6) (common)


    #
    # Public Functions
    #

    def add_method(self, method):

        # Attach method
        self._methods.append(method)


    def get_method(self, method_index):
        return self._methods[method_index]


    def database_output(self):

        message = "Box Database:\n"

        for method in self._methods:

            message += str(method)
            message += "\n"

            for package in method.packages:

                message += "    " 
                message += str(package)
                message += "\n"

                for box in package.boxes:

                    message += "        " 
                    message += str(box)
                    message += "\n"

        return message

#
# Private Functions
#





