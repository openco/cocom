from satchmo_store.shop.models import Order, OrderItem, Product, OrderVariable, OrderStatus
from stamps_lib import StampsLib, Constraints
from decimal import *
import postage_settings
#import time
import datetime
import logging

import sys, os
sys.path.append('/path/to/your/app')
os.environ['DJANGO_SETTINGS_MODULE'] = 'hello.settings'
from django.conf import settings



# For stack trace.
#import sys
#import traceback

class PostageVar:
    indicium = 'Postage-Status'
    indicium_flat = 'Postage-Rate-Flat'
    indicium_reg = 'Postage-Rate-Regional'
    indicium_weight = 'Postage-Rate-ByWeight'
    indicium_min = 'Postage-Rate-Selected'
    tracking = 'Tracking-Number'
    printing = 'Printing-Status'


# The options for status
# Currently:
# - get orders marked as 'Billed'
# - mark orders 'Blocked' if there is a problem
# - mark orders 'In Process' after getting the indicium
# - the print program marks as 'Completed' after printing
class StatusVar:
    temp = 'Temp'
    new = 'New'
    blocked = 'Blocked'
    inProcess = 'In Process'
    billed = 'Billed'
    shipped = 'Shipped'
    completed = 'Complete'
    cancelled = 'Cancelled'


class Box:
    dim1 = Decimal(0)
    dim2 = Decimal(0)
    dim3 = Decimal(0)

########################################################
#
# Global Variables
#
########################################################
logger = None
#sys_info = None

'''
def init_logging():
    global logger
    global handler

    # Initialize logging
    formatter = logging.Formatter(fmt='%(asctime)s\t%(name)s:\t%(levelname)s:\t%(message)s', datefmt=None)
    handler = logging.FileHandler('postage.log', mode='a', encoding=None, delay=False)
    handler.setFormatter(  formatter )
    logger = logging.getLogger('postage_daemon')
    logger.propagate = False
    logger.setLevel(logging.INFO)
    logger.addHandler( handler )
'''

def init_postage(incoming_handler):
    global logger
    global handler
    global stampsLib

    handler = incoming_handler

    # Initialize logging
    logger = logging.getLogger('postage')
    logger.propagate = False
    if (handler == None):
        print "No Log Handler.  Logging to stdout"
        #handler = logging.StreamHandler(sys.stdout)
        #self.logger.addHandler( logging.StreamHandler(sys.stdout) )
    else:
        logger.addHandler( handler )


    # Initialize stampslib. We send it our log handler
    # The second is for logging to screen
    stampsLib = StampsLib(handler, False)

'''
def postage_daemon():
    global sys_info
    global handler

    init_logging()

    print "Postage Daemon Running. See postage.log for details."
    log("Starting up...")

    # Initialize stampslib. We send it our log handler
    # The second is for logging to screen
    stampsLib = StampsLib(handler, False)

    # Detect CTLR-C
    try:

        # Loop forever
        while(True):

            # check for orders
            try:
                createIndicia(stampsLib)
            except Exception as e:
                log("Top level exception handler: " + str(e))
                sys_info = sys.exc_info()
                traceback.print_tb(sys_info[2])

            # wait a bit
            time.sleep(postage_settings.sleepSeconds)

    # Detect CTLR-C
    except KeyboardInterrupt as e:
        print "\nDetected KeyboardInterupt."
        log("Detected KeyboardInterupt.")

    print "Shutting down..."
    log("Shutting down...")
'''

def log(message):
    global logger
    #logger.info(str(message));
    print 'postage_deamon: ' + str(message)
    return

def createIndicia():
    global stampsLib

    # get new orders
    #orders = Order.objects.filter(status=StatusVar.billed)[:postage_settings.limit]
    #orders = Order.objects.filter(ship_country='US', shipping_method='USPS')
    orders = Order.objects.filter(status=StatusVar.new)[:postage_settings.limit]
    paid_orders = [x for x in orders if (x.balance==0)]

    orderQueue = []

    for order in paid_orders:

        # display the order
        # output_order(order)
        log(get_time() + 'Processing order: ' + str(order.id))

        #
        # ALEX START
        #

        # Get the payments
        payments = order.payments.all()

        # Check if there are payments
        if (len(payments) > 0):
            # Get the first payment
            payment = payments[0]
            if (payment.payment == 'PURCHASEORDER') and (payment.amount > 0):
                payment.amount = 0
                payment.save()
                log("Detected purchase order. Setting payment to 0.")



        #
        # ALEX END
        #

        # check for empty manditory fields
        success = verifyManditoryFields(order)
        if (not success):
            postage_error(order, "Manditory field missing.  Not handling.")

        # check for US as country
        elif (order.ship_country != 'US'):
            postage_error(order, "International shipping.  Not handling.")

        # check for USPS as shipping method
        elif (order.shipping_method != postage_settings.shippingMethod):
            postage_error(order, "Not USPS shipping.  Not handling.")

        # handle the order
        else:
            log("All good.  Adding to queue.")
            orderQueue.append(order)

    if len(orderQueue) == 0:
        log("No orders.")
        return

    # Login only once per group of transactions
    success = stampsLib.login()

    # Then handle the orders
    for order in orderQueue:
        log("====================================================")
        handle_order(stampsLib, order)
        log("====================================================")

# Called when we can't create the postage
def postage_error(order, message):
    log(message)
    # Put the error message in the order variable
    set_order_variable(order, PostageVar.indicium, message)
    # set status to blocked
    set_status(order, StatusVar.blocked)


def get_time():
    return datetime.datetime.now().isoformat() + ': '

def handle_order(stampsLib, order):

    boxConstraints = Constraints()

    # Get the total volume for the order
    success = get_order_volume(order, boxConstraints)
    output_variable("Total Volume (cubic inches)", boxConstraints.volume)
    if (not success):
        # One of the items had 0 volume.
        # The postage error has aalready been set
        return False
    if (boxConstraints.volume > postage_settings.volumeCutOff):
        postage_error( order, "Order Volume larger than largest box: " + str(round(boxConstraints.volume)) + " > " + str(postage_settings.volumeCutOff) )
        return False

    # Get the total weight for the order
    success = get_order_weight(order, boxConstraints)
    output_variable("Total Weight (lbs)", boxConstraints.weight)
    if (not success):
        # One of the items had 0 weight.
        # The postage error has aalready been set
        return False

    # Fill in the shipping addressee if it is missing.
    if ( order.ship_addressee.isspace() )  or (len(order.ship_addressee)==0):
        order.ship_addressee = order.contact.full_name
        log("No shipping addressee.  Using contact name: " + str(order.contact.full_name))

    # Fill in the billing addressee if it is missing.
    if ( order.bill_addressee.isspace() )  or (len(order.bill_addressee)==0):
        order.bill_addressee = order.contact.full_name
        log("No billing addressee.  Using contact name: " + str(order.contact.full_name))

    # Run the stampslib transaction
    success = stampsLib.create_indicium_transaction(
        str(order.id),
        order.ship_addressee,
        order.ship_street1,
        order.ship_street2,
        order.ship_city,
        order.ship_state,
        order.ship_postal_code,
        '',          # TODO phone
        order.contact.email,
        boxConstraints
        )

    # Check for errors
    if (not success):
        postage_error(order, stampsLib.error)
        return False

    rate_flat = stampsLib.rateInfoFlat.rateInfoString()
    rate_reg = stampsLib.rateInfoReg.rateInfoString()
    rate_weight = stampsLib.rateInfoWeight.rateInfoString()
    rate_min = stampsLib.rateInfoMin.rateInfoString()

    # Handle successful transaction
    set_status(order, StatusVar.inProcess)
    set_order_variable(order, PostageVar.indicium, 'Created')
    set_order_variable(order, PostageVar.indicium_flat, rate_flat)
    set_order_variable(order, PostageVar.indicium_reg, rate_reg)
    set_order_variable(order, PostageVar.indicium_weight, rate_weight)
    set_order_variable(order, PostageVar.indicium_min, rate_min)
    set_order_variable(order, PostageVar.tracking, stampsLib.trackingNumber)
    set_order_variable(order, PostageVar.printing, 'Pending')

    log("Indicium created.")
    return True

def get_order_weight(order, boxConstraints):

    # Get items
    items = order.orderitem_set.all()

    # Loop through items
    totalWeight = 0
    for item in items:

        # Get item weight
        weight = get_item_weight(item)

        # If weight is 0, it is an error
        if (weight == 0):
            postage_error(order, "Product has no weight: " + str(item))
            return False

        # Add item weight to total weight
        boxConstraints.weight += weight

    return True

def get_order_volume(order, boxConstraints):

    # Get items
    items = order.orderitem_set.all()

    # Loop through items
    totalVolume = 0
    for item in items:

        # Get item volume
        volume = get_item_volume(item, boxConstraints)

        log( item.product.name.encode('ascii', 'ignore') + ": Volume: " + str(volume) )

        # If volume is 0, it is an error
        if (volume == 0):
            postage_error(order, "Product has no volume: " + str(item))
            return False

        # Add item volume to total volume
        boxConstraints.volume += volume

    return True


def get_item_weight(item):
    weight = get_product_weight(item.product)

    # If we don't have a weight, try to get it from a parent product
    if ((weight == 0) and (item.product.has_variants)):
        # Does has_variants guarantee we can do this?  Who knows...
        weight = get_product_weight(item.product.productvariation.parent.product)

    # If we still don't have a weight, complain!
    if (weight == 0):
            return 0

    # Check for missing quantity
    if (item.quantity == None):
        log("WARNING: item has no quantity: " + str(item))
        return 0

    return (item.quantity * weight)

def get_item_volume(item, boxConstraints):

    # Get product volume
    volume = get_product_volume(item.product, boxConstraints)

    # If we don't have a volume, try to get it from a parent product
    if ((volume == 0) and (item.product.has_variants)):
        volume = get_product_volume(item.product.productvariation.parent.product, boxConstraints)

    # If we still don't have a volume, complain!
    if (volume == 0):
            return 0

    # Check for zero quantity
    if (item.quantity == None):
        log("WARNING: item has no quantity: " + str(item))
        return 0


    # Return volume for items
    #log( "Volume: " + str(volume) )
    #log( "Quantity: " + str(item.quantity) )
    return (item.quantity * volume)

# Returns product weight.  Returns 0 if product has no weight.
def get_product_weight(product):
    if (product.weight == None):
        return 0
    return product.weight

# Returns product volume.  Returns 0 if product has no volume.
def get_product_volume(product, boxConstraints):

    # TODO Message = Indicum not generated.  Check the order for details.

    # Get the dimensions.
    # But first check for zero length dimensions
    if (product.height == None) or (product.height <= 0):
        return 0
    elif (product.width == None) or (product.width <= 0):
        return 0
    elif (product.length == None) or (product.length <= 0):
        return 0

    # Assert: All dimensions are non-zero
    log( "Getting product volume. Product ID: " + str(product.id) )
    log( product.name.encode('ascii', 'ignore') + ": All dimensions are non-zero.")

    # Account for packaging
    height = product.height + Decimal(postage_settings.packingThickness)
    width = product.width + Decimal(postage_settings.packingThickness)
    length = product.length + Decimal(postage_settings.packingThickness)

    # Sort the dimensions
    dims = [height, width, length]
    dims.sort(reverse=True)

    # Expand the minimum box dimensions
    if boxConstraints.dim1 <  dims[0]:
        boxConstraints.dim1 = dims[0]
    if boxConstraints.dim2 <  dims[1]:
        boxConstraints.dim2 = dims[1]
    if boxConstraints.dim3 <  dims[2]:
        boxConstraints.dim3 = dims[2]

    squishFactor = Decimal(1)
    # Adjust for category
    if is_tea(product):
        squishFactor = Decimal(postage_settings.teaSquishFactor)

    if is_coffee(product):
        squishFactor = Decimal(postage_settings.coffeeSquishFactor)

    return height * width * length * squishFactor


def is_tea(product):
    if product.category.filter(name="Teas"):
        #print "TEA!"
        return True
    else:
        return False

def is_coffee(product):
    if product.category.filter(name="Coffee"):
        #print "COFFEE!"
        return True
    else:
        return False

#This is really useful probably not the most django way to do it though.
#Lets discus putting this in it's own file so I wouldn't have to import it from this... though I supose that isn't to bad an option
def set_order_variable(order, varKey, varValue):

    # Get the OrderVariable object
    vars = order.variables.filter(key=varKey)

    var = None

    try:
        if (len(vars) == 0):
            # If it doesn't exist, create one
            var = order.variables.create(key=varKey, value=varValue)
        else:
            # If it does exist, set it
            var = vars[0]
            var.value = varValue
            var.save()
    except Warning, e:
        log(e)

    log(str(var))

# Adds a new status
def set_status(order, newStatus):
    stat = order.orderstatus_set.create(status=newStatus)
    log('Status: ' + str(stat))

def verifyManditoryFields(order):
    # fields to check
    fields = (order.id, order.ship_street1, order.ship_city, order.ship_postal_code, order.ship_country, order.shipping_method)

    # loop through the fields
    for field in fields:
        success = verify_field(field)
        if (not success):
            return False
        # else:
            # print str(field) + " is OK"

    # all is well
    return True

def verify_field(value):
    if (value == None):
        return False
    return True

def output_variable(name, value):
    if (value != None):
        log( name + ' ' + str(value) )
    else:
        log( name + ' [EMPTY]' )

def output_order(order):
    output_variable("ID: ", order.id)
    output_variable("Name: ", order.ship_first_name +  " " + order.ship_last_name)
    output_variable("Address1: ", order.ship_street1)
    output_variable("Address2: ", order.ship_street2)
    output_variable("City: ", order.ship_city)
    output_variable("State: ", order.ship_state)
    output_variable("ZIP: ", order.ship_postal_code)
    output_variable("Country: ", order.ship_country)
    output_variable("Shipping Method: ", order.shipping_method)
    output_variable("Status: ", order.status)
    output_variable("Notes: ", order.notes)

# If this file is being executed directly, start the postage daemon
if __name__ == "__main__":
    postage_daemon()
