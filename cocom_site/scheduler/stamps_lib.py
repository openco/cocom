##############################################################################
#
# Imports
#     requires "suds" for communicating with the SOAP server.
#     requires "twill" for downloading the label image.
#
##############################################################################

# External Imports
import sys
import traceback
import logging
import os
from decimal import *
from datetime import date,datetime

# Suds is a SOAP client to connect to stamps.com
import suds
from suds.client import Client, WebFault
from suds.cache import NoCache

# Urllib2 is used to download the image of the indicium
import urllib2
# replacing twill with urllib2
#from twill import get_browser

# Internal Imports
import stamps_settings
from stamps_boxes import StampsBoxes


class Constraints:
    def __init__(self):
        self.weight = Decimal(0)
        self.boxWeight = Decimal(0)
        self.volume = Decimal(0)
        self.dim1   = Decimal(0)
        self.dim2   = Decimal(0)
        self.dim3   = Decimal(0)
    def get_weight(self):
        return self.weight + self.boxWeight

class RateInfo:
    def __init__(self):
        self.package_name = ""
        self.rate = None
        self.cost = Decimal(0)
        self.box = None
        self.boxFound = False
        self.rateFound = False
    def __str__(self):
                return "Method: " + self.name

    def rateInfoString(self):
        if (not self.boxFound):
            return "Items do not fit."

        if (not self.rateFound):
            cost = "No Rate found."
        else:
            cost = "$" + str(self.cost)

        name =  self.package_name
        box = self.box.description

        return cost + "  " + name + "  " + box

        #['package_name'] + ": " + str(rateInfo['cost']))

class StampsLib:


    ##############################################################################
    #
    # Public Variables
    #
    ##############################################################################
    error = ''
    warning = ''
    trackingNumber = ''
    logger = None
    logToScreen = False

    rateInfoFlat = None
    rateInfoReg = None
    rateInfoWeight = None
    rateInfoMin = None

    ##############################################################################
    #
    # Private Variables
    #
    ##############################################################################
    _client = None
    _boxes = None

    # A string from the previous repsonse 
    # to send with the next request.
    # Guaratees authentication and message order.
    _authenticator = None

    _balance = None

    _rates = None
    _rateDict = {}

    ##############################################################################
    #
    # Private Functions
    #
    ##############################################################################


    # Initialization
    def __init__(self, handler, logToScreen):


        # Initialize logging
        self.logToScreen = logToScreen
        self.logger = logging.getLogger('stamps_lib')
        self.logger.propagate = False
        if (handler == None):
            print "No Log Handler.  Logging to stdout"
            #handler = logging.StreamHandler(sys.stdout)
            #self.logger.addHandler( logging.StreamHandler(sys.stdout) )
        else:
            self.logger.addHandler( handler )

        self.log("Initializing...")

        # SUDS Logging
        #logging.basicConfig(level=logging.INFO)
        #logging.getLogger('suds.client').setLevel(logging.DEBUG)
        #logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)
        #logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)

        # Create a suds client
        self._client = Client(stamps_settings.wsdl)
        # Make calls to: https://swsim.testing.stamps.com/swsim/SwsimV24.asmx
        #_client.set_options(location='https://swsim.testing.stamps.com/swsim/SwsimV24.asmx')
        #_client.set_options(cache=NoCache())

        self._boxes = StampsBoxes()
        self.log(self._boxes.database_output())


    #
    # Output a log message.
    #   Currently this goes to stdout.
    #   Currently this has lines around it to it can be seen
    #       amongst the XML debug output.
    #
    def log(self, message):
        self.logger.info(str(message))
        if self.logToScreen:
            print 'stamps_lib: ' + str(message)

    def set_warning(self, message):
        self.warning = "Warning: " + str(message)
        self.log(self.warning)

    def set_error(self, message):
        self.error = "Error: " + str(message)
        self.log(self.error)

    #
    # Logs in to the server and gets account info.
    #   returns boolean success, authenticator, and a balance object
    #
    def _login(self):

        credentials = self._client.factory.create('Credentials')
        credentials.IntegrationID = stamps_settings.integrationID
        credentials.Username = stamps_settings.username
        credentials.Password = stamps_settings.password

        # Make the SOAP call
        try:
            result = self._client.service.AuthenticateUser(credentials)
            result = self._client.service.GetAccountInfo(result.Authenticator, None)
        except WebFault, e:
            self.set_error("stamps.com: " + str(e))
            return False

        self._balance = result.AccountInfo.PostageBalance
        self._authenticator = result.Authenticator

        self.log("Balance: " + str(self._balance.AvailablePostage))

        return True

    #
    # Adds funds to the account balance.
    #    amount must be greater than or equal to 10.
    #    returns boolean success and authenticator
    #
    def purchasePostage(self, amount):

        #TODO check amount is at least 10

        # Make the SOAP call
        try:
            result = self._client.service.PurchasePostage(
                None, 
                self._authenticator, 
                amount, 
                self._balance.ControlTotal)
        except WebFault, e:
            self.set_error("SOAP:" + str(e))
            return False

        self._authenticator = result.Authenticator
        return True

    #
    # Alters the address to meet US Postal System standards.
    #    modifies global address
    #    returns boolean success
    #
    def cleanseAddress(self, toAddress):

        # Handle extended zip codes
        if (len(toAddress.ZIPCode) > 5):
            # trancate to 5 digits
            self.log("Truncating a long ZIP code..." + str(toAddress.ZIPCode))
            toAddress.ZIPCode = toAddress.ZIPCode[0:5]

        self.log(toAddress)

        # Make the SOAP call
        try:
            result = self._client.service.CleanseAddress(
                self._authenticator, 
                None, 
                toAddress)
        except WebFault, e:
            self.set_error("SOAP:" + str(e))
            return (False, toAddress)

        self._authenticator = result.Authenticator
        toAddress = result.Address

        self.log("CleanseAddress Response: ")
        self.log(result)

        # If address was matched, accept changes and return it.
        if result.AddressMatch:
            # The cleanse hash is used.
            # The override hash is cleared.
            toAddress.OverrideHash = None
            return (True, toAddress)

        # Assert: The address wasn't matched

        # If the city, state, and zip are consistent, 
        # use the override so that the address can used.
        # LM requested that these be let through in Oct 2013
        if result.CityStateZipOK:
            # The clense hash is empty.
            # The override hash is used.
            self.set_warning("Street address cannot be verified.  City, State, and ZIP are OK.")
            return (True, toAddress)

        # Assert: The address is a total fail.
        self.set_error("The address is invalid and cannot be automatically corrected.")
        return (False, toAddress)


    #
    # Gets the cost of the postage from the weight and ZIP codes.
    #    modifies global rate
    #    returns boolean success
    #
    def getRates(self, fromAddress, toAddress, constraints):

        # clear the rate dictionary
        self._rateDict = {}

        # get the current date
        today = date.today()

        packageType = self._client.factory.create('PackageTypeV6').Package
        # serviceType = client.factory.create('ServiceType')
        serviceType = 'US-PM'

        rate = self._client.factory.create('RateV9')
        rate.FromZIPCode = fromAddress.ZIPCode
        rate.ToZIPCode = toAddress.ZIPCode
        rate.ServiceType = serviceType
        # rate.PackageType = packageType
        rate.WeightLb = str(constraints.get_weight())
        rate.WeightOz = '0'
        rate.ShipDate = today.isoformat()
        #rate.InsuredValue = "10"
        #rate.Width = "10"
        #rate.Length = "10"
        #rate.Height = "10"

        # Make the SOAP call
        try:
            result = self._client.service.GetRates(self._authenticator, None, rate)
        except WebFault, e:
            self.set_error("SOAP:" + str(e))
            return False

        self._authenticator = result.Authenticator

        # Check if we got a rate
        numRates = len(result.Rates)
        if numRates > 0:
            self._rates = result.Rates.Rate
        else:
            self._rates = None
            return False

        # Build a rate dictionary
        self.log("All Rates:")
        for rate in self._rates:
            self.log("\t" + rate.PackageType + ", " + str(rate.Amount))
            self._rateDict[rate.PackageType] = rate

        return True

    #
    # Get the box weight
    # (estimated from the brown box weights since those are the
    # ones most affected by weight)
    #
    def getBoxWeight(self, constraints):

        # The the by-weight shipping method
        method = self._boxes.get_method(2)

        # Get the smallest box that fits
        box = method.get_fit( constraints.weight, constraints.volume, constraints.dim1, constraints.dim2, constraints.dim3)

        # Add the box weight to the constraints
        if box:
            constraints.boxWeight = box.boxWeight

        return


    #
    # Figure out what rate to use.
    # "The Triple Shipping Method Check"
    # - Figure out which package type to use for each shipping method.
    # - Figure out which box to use for each shipping method.
    # - Get the cost for the three package types
    # - Choose the cheapest cost
    #
    def chooseRate(self, constraints):

        # Get shipping methods
        method_flat = self._boxes.get_method(0)
        method_reg = self._boxes.get_method(1)
        method_weight = self._boxes.get_method(2)

        self.log("Shortlist:")

        # Get rate info from the box database
        self.rateInfoFlat =   self.choosePackage(method_flat, constraints)
        self.rateInfoReg =    self.choosePackage(method_reg, constraints)
        self.rateInfoWeight = self.choosePackage(method_weight, constraints)

        self.debug_rate_info(self.rateInfoFlat)
        self.debug_rate_info(self.rateInfoReg)
        self.debug_rate_info(self.rateInfoWeight)

        # Find the minimum rate
        self.rateInfoMin = self.minimum_cost_rate(self.rateInfoFlat, self.rateInfoReg)
        self.rateInfoMin = self.minimum_cost_rate(self.rateInfoMin, self.rateInfoWeight)

        # Check if none of the rates worked
        if (self.rateInfoMin == None):
            self.set_error("Order doesn't fit in any box. Check weight and volume.")
            return False

        return True

    #
    # Called by chooseRate
    #
    def choosePackage(self, method, constraints):

        # Create a new rate info
        rateInfo = RateInfo()

        # Get the smallest box that fits
        rateInfo.box = method.get_fit( constraints.weight, constraints.volume, constraints.dim1, constraints.dim2, constraints.dim3)

        # Check if no box was found
        if (rateInfo.box == None):
            rateInfo.boxFound = False
            self.log ( method.name + ": " + "No package found." )
            return rateInfo

        # Get the package name
        rateInfo.boxFound = True
        rateInfo.package_name = rateInfo.box.package.name

        # Get the cost
        if (rateInfo.package_name in self._rateDict):
            rateInfo.rateFound = True
            rate = self._rateDict[rateInfo.package_name]
            rateInfo.rate = rate
            rateInfo.cost = rate.Amount
        else:
            rateInfo.rateFound = False

        return rateInfo

    #
    # Called by chooseRate
    #
    def minimum_cost_rate(self, rateInfo1, rateInfo2):
        if (not rateInfo1.rateFound):
            return rateInfo2
        elif (not rateInfo2.rateFound):
            return rateInfo1
        elif (rateInfo1.cost <= rateInfo2.cost):
            return rateInfo1
        else:
            return rateInfo2

    #
    #
    #
    def debug_rate_info(self, rateInfo):
        if (rateInfo == None):
            return

        self.log("\t" + rateInfo.rateInfoString())


    #
    # Finds an addon in the rate addon array.
    #     returns None if not found.
    #
    def getAddOn(self, addOns, type):
        for addOn in addOns:
            if addOn.AddOnType == type:
                return addOn
        return None

    #
    # Creates an indicium (postage label).
    #    modifies global indicium
    #    returns boolean success
    #
    def createIndicium(self, fromAddress, toAddress, transactionID, toEmail):

        rate = self.rateInfoMin.rate

        year = str(datetime.utcnow().year)
        month = str(datetime.utcnow().month).zfill(2)
        day = str(datetime.utcnow().day).zfill(2)
        hour = str(datetime.utcnow().hour).zfill(2)
        month = str(datetime.utcnow().month).zfill(2)
        minute = str(datetime.utcnow().minute).zfill(2)
        second = str(datetime.utcnow().second).zfill(2)
        datetimeString = year + month + day + hour + minute + second

        # Replace the full addons array with only the addons we want.
        deliveryConfirmation = self.getAddOn(rate.AddOns.AddOnV2, "US-A-DC")
        hiddenPostage = self.getAddOn(rate.AddOns.AddOnV2, "SC-A-HP")
        rate.AddOns.AddOnV2 = [ deliveryConfirmation, hiddenPostage ]
        #self.log("Adons: " + str(rate.AddOns.AddOnV2))

	self.log(toAddress)

        # Build text
        #message = "Order #: " + transactionID + " Rate: $" + str(rate.Amount) + " \n " + self.rateInfoMin.package_name + " " + self.rateInfoMin.box.description,
        message = "Order #: " + transactionID + " \n " + self.rateInfoMin.package_name + " " + self.rateInfoMin.box.description,

        # Make the SOAP call
        try:
            result = self._client.service.CreateIndicium(
                self._authenticator, 
                None, 
                transactionID + datetimeString, 
                None, 
                rate, 
                fromAddress, 
                toAddress,
                None, None, 
                stamps_settings.testing, # Sample Fake Postage
                None, None, 
                message,
                None,
                #toEmail, None, None, None, True, True,
                None, None, None, None, True, True, 
                None, None, None, None, 
                "true")
        except WebFault, e:
            self.set_error("SOAP:" + str(e))
            return (False, None)

        self._authenticator = result.Authenticator
        indicium = result

        self.trackingNumber = indicium.TrackingNumber

        return (True, indicium)


    def saveIndicium(self, indicium, transactionID):

        try:

            textFilename = stamps_settings.postageDir + "/trans" + transactionID + ".txt"
            labelFilename = stamps_settings.postageDir + "/trans" + transactionID + ".png"

            self.log("Saving " + textFilename + "...")
            textFile = open(textFilename, "w")
            textFile.write(str(indicium))
            textFile.close()

            # Remove the symlink
            if os.path.exists(labelFilename):
                self.log("Removing symlink " + labelFilename + "...")
                os.remove(labelFilename)

            # The the image file from stamps.com
            self.log("Connecting to stamps.com URL...")
            data = self.get_file( indicium.URL )

            # Save the image file
            self.log("Saving " + labelFilename + "...")
            labelFile = open(labelFilename, "wb")
            labelFile.write(data)
            labelFile.close()

        except Exception, e:
            self.log("Error while saving files: " + textFilename + " or " + labelFilename + ":" + str(e))
            sys_info = sys.exc_info()
            traceback.print_tb(sys_info[2])

        # Return true even if there were errors.  
        # This prevents an infinite loop where we keep purchasing postage.
        return True

    # A helper function to download binary files using URLlib.
    def get_file(self, url):

        opener = urllib2.build_opener()

        page = opener.open(url)
        data = page.read()
        page.close()
        opener.close()

        return data



    ##############################################################################
    #
    # Public Functions
    #
    ##############################################################################

    # Public method to login to stamps.com server
    def login(self):

        self.log("Logging in...")
        # Call the private method
        success = self._login()
        if success:
            self.log("Login Successful.")
        else:
            self.log("Login Failed.")
            return False

        return True

    # Public method to create an indicium
    # Pre-reqeuisites: must have already called login()
    def create_indicium_transaction(
        self,
        transactionID,
        toName,
        toAddress1,
        toAddress2,
        toCity,
        toState,
        toZIP,
        toPhoneNumber,
        toEmail,
        boxConstraints):

        self.log("Transaction ID: " + transactionID)
        self.log("Weight: " + str(boxConstraints.weight))
        self.log("Volume: " + str(boxConstraints.volume))
        self.log("dim1: " + str(boxConstraints.dim1))
        self.log("dim2: " + str(boxConstraints.dim2))
        self.log("dim3: " + str(boxConstraints.dim3))

        # Check if we're authenticated
        if (not self._authenticator):
            # if not, call login again
            success = self._login()
            if success:
                self.log("Login Successful.")
            else:
                self.log("Login Failed.")
                return False

        # Create the from address
        fromAddress = self._client.factory.create('Address')
        fromAddress.FullName = stamps_settings.fromName
        fromAddress.Address1 = stamps_settings.fromAddress1
        fromAddress.City = stamps_settings.fromCity
        fromAddress.State = stamps_settings.fromState
        fromAddress.ZIPCode = stamps_settings.fromZIP
        fromAddress.PhoneNumber = stamps_settings.fromPhoneNumber

        # Create the from address
        toAddress = self._client.factory.create('Address')
        toAddress.FullName = toName
        toAddress.Address1 = toAddress1
        toAddress.Address2 = toAddress2
        toAddress.City = toCity
        toAddress.State = toState
        toAddress.ZIPCode = toZIP
        toAddress.PhoneNumber = toPhoneNumber


        # Cleanse Address
        self.log("Cleansing Address...")
        (success, toAddress) = self.cleanseAddress(toAddress)
        if success:
            self.log("Address Cleansed.")
        else:
            self.log("Failed to cleans address.")
            self._authenticator = None
            return False

        # Get box weight
        self.getBoxWeight(boxConstraints)
        self.log("Box Weight: " + str(boxConstraints.boxWeight))

        # Get rates
        self.log("Getting Rates...")
        success = self.getRates(fromAddress, toAddress, boxConstraints)
        if success:
            self.log("Retrieved " + str(len(self._rates)) + " rate(s).")
            # self.log("Rate: $" + str(rate.Amount))
            # self.cost = "$" + str(rate.Amount)
        else:
            self.log("Failed to get rate.")
            self._authenticator = None
            return False

        # Process rates
        success = self.chooseRate(boxConstraints)
        if success:
            self.log("Rate selected: ")
            self.debug_rate_info(self.rateInfoMin)
        else:
            return False

        # for testing
        # return True

        # Create the indicium/label
        self.log("Creating Indicium (Label)...")
        (success, indicium) = self.createIndicium(fromAddress, toAddress, transactionID, toEmail)
        if success:
            self.log("Indicium (Label) created.")
        else:
            self.log("Failed to create indicium (label).")
            self._authenticator = None
            return False

        # Save the image and txt file
        self.log("Saving File...")
        success = self.saveIndicium(indicium, transactionID)
        if success:
            self.log("Files saved.")
        else:
            self.log("Failed to save files.")
            return False

        # Exit with success
        return True

    def test_boxes(self, boxConstraints):

        # Process rates
        success = self.chooseRate(boxConstraints)
        if success:
            self.log("Rate selected: ")
            self.debug_rate_info(self.rateInfoMin)
        else:
            return False

    def purchase_postage_transaction(self, amount):

        # Login
        #self.log("Loging in...")
        #(success) = self.login()
        #if success:
        #    self.log("Login Successful.")
        #else:
        #    self.log("Login Failed.")
        #    return False

        # Purchase some postage
        self.log("Purchasing Postage...")
        success = self.purchasePostage(amount)
        if success:
            self.log("Purchase Postage Successful.")
            return True
        else:
            self.log("Purchase Postage Failed.")
            self._authenticator = None
            return False

        return True

    # Outputs the SUDS parse of the WSDL
    def output_wsdl(self):
        print _client
