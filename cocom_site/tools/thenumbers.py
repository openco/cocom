import os
from django.db.models import Q
from django.db.models import Sum
from satchmo_store.shop.models import Order
from satchmo_store.shop.models import Cart
import datetime


def break_down(orders):
    product_orders={'category total':0}
    wholesale_total = 0
    consumer_total = 0
    for i in orders:
        try:
            i.contact.user.groups.count() > 1
            wholesale_total = wholesale_total + i.total
        except:
            consumer_total = consumer_total + i.total
            for item in i.orderitem_set.all():
                if item.category == "Gear":
                    product_orders['category total']+=item.line_total
                    try:
                        product_orders[str(item.product.name)]+=item.line_total
                    except:
                        product_orders[str(item.product.name)]=item.line_total
    print "    Total order value:    " + str(orders.aggregate(Sum('total'))['total__sum'])
    print "    Wholesale order total:" + str(wholesale_total)
    print "    Consumer order total: " + str(consumer_total)
    print ""
    print product_orders
    print ""



print "All orders:"
break_down(Order.objects.exclude(status__exact=''))
print "Jan orders:"
break_down(Order.objects.filter(time_stamp__year=2013,time_stamp__month=1).exclude(status__exact=''))
print "Feb orders:"
break_down(Order.objects.filter(time_stamp__year=2013,time_stamp__month=2).exclude(status__exact=''))
print "Mar orders:"
break_down(Order.objects.filter(time_stamp__year=2013,time_stamp__month=3).exclude(status__exact=''))
print "Apr orders:"
break_down(Order.objects.filter(time_stamp__year=2013,time_stamp__month=4).exclude(status__exact=''))
print "May orders:"
break_down(Order.objects.filter(time_stamp__year=2013,time_stamp__month=5).exclude(status__exact=''))
