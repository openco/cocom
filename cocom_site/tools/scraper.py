#!/usr/bin/python
# coding: utf-8

# To connect to wordpress db
import MySQLdb

# Regular Expressions
import re

# To connect to django-cms
import os
import sys
#sys.path.append('.')
sys.path.append(os.path.expanduser('~/cocom/cocom_site'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cocom_site.settings")
from cms.models import Page, Title, Placeholder
from cms.api import create_page, create_title, add_plugin, publish_page
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from client_event.models import Event
from product.models import Price, ProductTranslation

# For date handling 
from datetime import date

# To get command-line parameters
import getopt

    

class Main:

    def __init__(self, argv):

        # Create interface objects
        self.language = Language()
        self.wordpress = Wordpress()
        self.django = Django()

        # Get command line args
        try:
            opts, args = getopt.getopt(argv,"", ['help','delete','scrape'])
        except getopt.GetoptError:
            self.usage()
            sys.exit(2)

        if not opts:
            self.usage()
            sys.exit(2)

        # Parse command line args
        for opt, arg in opts:
            if opt == '--help':
                self.usage()
                sys.exit(0)
            elif opt == '--delete':
                self.django.clear_previous_pages()
            elif opt == '--scrape':
                pages = self.wordpress.pull_pages()
                self.process_pages(pages)

    def usage(self):
        print "Wordpress Scraping Tool.  Options:"
        print "    --delete     delete pages from the database"
        print "    --scrape     find absolute links"
        print "    --help       help"

    def process_pages(self, rows):

        # Pages to exclude
        exclude = []

        # Maps wordpress ids to django ids
        mapping = {}
        # Maps django ids to parent wordpress ids
        parents = {}

        # init language stats
        count = {}
        count['total'] = 0
        count['english'] = 0
        count['french'] = 0
        count['complete'] = 0
        count['english_only'] = 0
        count['french_only'] = 0
        count['no_content'] = 0
        count['missing_french_title'] = 0

        print "Pulled " + str(len(rows)) + " pages."
    
        for row in rows:
            # Get data
            id = row[0]
            parent_id = row[4]
            title = row[1]
            content = row[2]

            # output
            print "============================================="
            print id, parent_id, title.encode('UTF8')
            #print content.encode('UTF8')

            if id in exclude:
                print "Excluded"
                continue

            # Make page object
            page_data = PageData(id, parent_id, self.language)
            page_data.set_title(title)
            page_data.set_content(content)

            # update stats
            count['total'] += 1
            if page_data.english_content and page_data.french_content:
                count['complete'] += 1
                count['english'] += 1
                count['french'] += 1
            elif page_data.english_content:
                count['english_only'] += 1
                count['english'] += 1
            elif page_data.french_content:
                count['french_only'] += 1
                count['french'] += 1
                print "FRENCH_ONLY"
            else:
                count['no_content'] += 1
                print "NO_CONTENT"

            if page_data.french_content and not page_data.french_title:
                count['missing_french_title'] += 1
                print "MISSING_FRENCH_TITLE"

            if page_data.english_content and not page_data.english_title:
                print "MISSING_ENGLISH_TITLE"

            # choose a template
            template = u'template_1.html'

            # create the page
            django_id = self.django.cms_page(page_data, template)

            # Update mappings
            parents[django_id] = parent_id
            mapping[id] = django_id

        print "Structuring..."
        # A list of top-level pages
        top_level = []
        for django_id, wp_parent_id in parents.iteritems(): 

            # Check for WP parent of 0
            if wp_parent_id == 0:
                # No parent
                print "\tDJ: " + str(django_id) + " => WP: " + str(wp_parent_id) + " => DJ: Top-Level"
                top_level.append(django_id)
                #self.django.cms_set_parent(django_id, django_home_id)
            else:
                # Found a parent
                django_parent_id = mapping[wp_parent_id]
                print "\tDJ: " + str(django_id) + " => WP: " + str(wp_parent_id) + " => DJ:" + str(django_parent_id)
                self.django.cms_set_parent(django_id, django_parent_id)
        print "Done Structuring."


        print "Publishing..."
        self.django.cms_publish_list(top_level)
        print "Done Publishing."

        print "Stats:"
        print "\tTotal:        " + str(count['total'])
        print "\tEnglish:      " + str(count['english'])
        print "\tFrench:       " + str(count['french'])
        print "\tComplete:     " + str(count['complete'])
        print "\tEnglish Only: " + str(count['english_only'])
        print "\tFrench Only:  " + str(count['french_only'])
        print "\tNo Content:   " + str(count['no_content'])
        print "\tMissing Fr Title: " + str(count['missing_french_title'])


    def add_para(self, text):
        return text

class PageData:

    def __init__(self, id, parent_id, language):
        self.id = id
        self.parent_id = parent_id
        self.language = language
        
        self.title = None
        self.content = None
        self.english_title = None
        self.french_title = None
        self.english_content = None
        self.french_content = None

    def set_title(self, title):
        self.title = title

        print "Processing Title..."
        [self.english_title, self.french_title] = self.language.parse(title)

        if self.english_title:
            print "EN: " + self.english_title.encode('UTF8')
        if self.french_title:
            print "FR: " + self.french_title.encode('UTF8')

    def set_content(self, content):
        self.content = content

        print "Processing Content..."
        [self.english_content, self.french_content] = self.language.parse(content)

        self.english_content = self.set_para(self.english_content)

    def set_para(self, content):
        if not content:
            return None
        lines = content.split('\n')
        output_lines = []
        for line in lines:
            output_lines.append('<p>' + line + '</p>')

        return ''.join(output_lines)



class Wordpress:

    def __init__(self):
        self.connect()

    def connect(self):
        self.db = MySQLdb.connect(host="localhost", # your host, usually localhost
                     user="scraper", # your username
                     passwd="scraper", # your password
                     db="qmea", # name of the data base
                                 charset='utf8') 

    def pull_pages(self):
        # you must create a Cursor object. It will let
        #  you execute all the query you need
        cur = self.db.cursor() 

        # Grabs all the posts
        # the post_type='page' is there because there are a lot of duplicate posts with post_type='revision'.
        cur.execute("SELECT ID, post_title, post_content, post_status, post_parent, post_type FROM wp_posts where post_type='page' or post_type='post'")

        return cur.fetchall()

    def pull_events(self):
        # you must create a Cursor object. It will let
        #  you execute all the query you need
        cur = self.db.cursor() 

        # Grabs all the posts
        # the post_type='page' is there because there are a lot of duplicate posts with post_type='revision'.
        cur.execute("SELECT ID, post_title, post_content, post_status, post_parent, post_type FROM wp_posts where post_type='pt_event' and post_status='publish' ")

        return cur.fetchall()

class Language:

    def __init__(self):
        self.re_en = re.compile('<!--:en-->(.*?)<!--:-->', re.DOTALL)
        self.re_fr = re.compile('<!--:fr-->(.*?)<!--:-->', re.DOTALL)
        self.empty = re.compile('^\s*$')

    def parse(self, text):

        match_en = self.re_en.search(text)
        match_fr = self.re_fr.search(text)

        if match_en == None:
            print "\tEN: NOT Detected"
            english = None
        else:
            print "\tEN: Detected"
            english = match_en.group(1)

        if match_fr == None:
            print "\tFR: NOT Detected"
            french = None
        else:
            print "\tFR: Detected"
            french = match_fr.group(1)

        # If we didn't detect any lanuages, default to english
        if not english and not french:
            english = text

        # If only whitespace, convert to None
        if english and self.empty.match(english):
            english = None

        if french and self.empty.match(french):
            french = None

        return [english, french]


class Django:

    def __init__(self):
        self.user = User.objects.get(pk=1)
        self.placeholder_fail = 0

    # Deletes the script created pages from the django db
    def clear_previous_pages(self):
        print "Clearing previous pages..."
        pages = Page.objects.filter(created_by='script',publisher_is_draft=True)
        for page in pages:
            print "\tDeleting" + " " + str(page.id) + " " 

            try:
                titles = page.title_set.all()
                if len(titles) != 0:
                    print "\t\t" + titles[0].title
                page.delete()
            except Page.DoesNotExist:
                print "\t\tAlready gone."
        print "Done clearing pages."

    def cms_page(self, page_data, template):
        
        if template == None:
            template = u'template_1.html'

        if page_data.english_title == None or page_data.english_title == '':
            print "WARNING: No English Title.  Need to handle this."
            sys.exit(0)

        if page_data.french_content and not page_data.french_title:
            page_data.french_title = page_data.english_title

        # Create Page and English Title
        page = create_page(page_data.english_title, template, u'en', in_navigation=False)

        # Add French Title
        if page_data.french_title != None:
            create_title(u'fr', page_data.french_title, page)

        # Get place holder
        placeholder = page.placeholders.get(slot=u'template_content')

        # Add English Content
        if page_data.english_content != None:
            add_plugin(placeholder, 'TextPlugin', u'en', body=page_data.english_content)

        # Add French Content
        if page_data.french_content != None:
            add_plugin(placeholder, 'TextPlugin', u'fr', body=page_data.french_content)

        # Save to database        
        page.in_navigation = True
        page.save()

        print "Page ID: " + str(page.id)

        # Old code that didn't use the cms API
        # Publish Page
        #p = Page(site_id=1L)
        #t = Title(title = u'test1', slug = u'test1', language=u'en')
        #t2.page_id = p.pk

        return page.id

    def cms_add_placeholder_content(self, page_id, page_data):
        # Get the page
        page = Page.objects.get(pk=page_id)

        # Get place holder for trip template
        if 'Description' in page_data.english_title:
            slot = u'description'
        elif 'Photos' in page_data.english_title:
            slot = u'Photos'
        elif 'Kayak' in page_data.english_title:
            slot = u'kayak_canoe'
        elif 'Packing List' in page_data.english_title:
            slot = u'packing_list'
        elif 'Getting There' in page_data.english_title:
            slot = u'getting_there'
        elif 'FAQ' in page_data.english_title:
            slot = u'faq'
        elif 'Regional Info' in page_data.english_title:
            slot = u'regional_info'
        # Get place holder for trip template
        elif 'WMA' in page_data.english_title:
            slot = u'wma'
        elif 'Itiner' in page_data.english_title:
            slot = u'itinerary'
        elif 'Directions' in page_data.english_title:
            slot = u'directions'
        elif 'Camping' in page_data.english_title:
            slot = u'river_camping'
        elif 'Pacuare' in page_data.english_title:
            slot = u'pacuare_river_camp'
        
        else:
            slot = u'template_content'
            self.placeholder_fail += 1
            print "Failed: " + page_data.english_title

        try:
            placeholder = page.placeholders.get(slot=slot)
        except Placeholder.DoesNotExist:
            placeholder = page.placeholders.get(slot=u'template_content')
        

        # Add English Content
        if page_data.english_content != None:
            add_plugin(placeholder, 'TextPlugin', u'en', body=page_data.english_content)

        # Add French Content
        if page_data.french_content != None:
            add_plugin(placeholder, 'TextPlugin', u'fr', body=page_data.french_content)

    def cms_placeholder_stats(self):
        print "Failed: " + str(self.placeholder_fail)

    def cms_set_parent(self, page_id, parent_page_id):
        if page_id == parent_page_id:
            print "Can't set parent to self."
            return
        page = Page.objects.get(pk=page_id)
        parent_page = Page.objects.get(pk=parent_page_id)
        page.parent = parent_page
        page.save()

    # Add a page to naviation
    def cms_set_navigation(self, id, in_nav):
        page = Page.objects.get(pk=id)
        page.in_navigation = in_nav
        page.save()

    # Publish a list of pages
    def cms_publish_list(self, page_ids):
        for django_id in page_ids:
            page = Page.objects.get(pk=django_id)
            self.cms_publish_tree(page)

    # Publish a tree of pages
    def cms_publish_tree(self, page):
        print "\tPublishing " + str(page.id)
        publish_page(page, self.user)
        for child in page.children.all():
            self.cms_publish_tree(child)

    def cms_get_page_id(self, title):
        p = Page.objects.filter(title_set__title=title)[0]
        return p.id

    def client_event(self, start_date, end_date, english_name, french_name, page_data, location, cost):

        e = Event()
        e.site = Site.objects.get(pk=1)
        e.name = english_name
        e.start_date = start_date
        e.end_date = end_date
        e.location = location
        e.province_code = self.detect_province_code(location)
        e.country_code = self.detect_country_code(location)
        e.save()

        # Add French Name
        if french_name:
            trans = ProductTranslation()
            trans.languagecode = u'fr'
            trans.name = french_name
            trans.product = e
            trans.save()

        # Add English Content
        if page_data.english_content != None:
            add_plugin(e.placeholder_1, 'TextPlugin', u'en', body=page_data.english_content)

        # Add French Content
        if page_data.french_content != None:
            add_plugin(e.placeholder_1, 'TextPlugin', u'fr', body=page_data.french_content)

        if cost:
            p = Price()
            p.price = cost
            p.product = e
            p.save()
        #t = Title(title = u'test1', slug = u'test1', language=u'en')
        #t2.page_id = p.pk

    def detect_province_code(self, location):
        if 'Ontario' in location or 'ON' in location:
            return 'ON'
        if 'Quebec' in location or 'Gatineau' in location or 'QC' in location:
            return 'QC'
        if 'British Columbia' in location or 'BC' in location:
            return 'BC'
        if 'Manitoba' in location:
            return 'MB'

        return None

    def detect_country_code(self, location):
        
        if self.detect_province_code(location):
            return 'CA'

        if 'Argentina' in location:
            return 'AR'
        if 'Costa Rica' in location:
            return 'CR'

        return None

# Instantiate Main object
main = Main(sys.argv[1:])
