#!/usr/bin/env python

from satchmo_store.shop.models import Product

def get_name(product):
    return '"' + product.name.encode('ascii', 'replace') + '"'

def get_id(product):
    return str(product.id)

def get_link(product):
    return 'https://site.url/admin/product/product/' + str(product.id)

def get_weight(product):
    if ( (product.weight == None) and (product.has_variants) ):
        return str(product.productvariation.parent.product.weight)
    return str(product.weight)

def get_length(product):
    if ( (product.length == None) and (product.has_variants) ):
        return str(product.productvariation.parent.product.length)
    return str(product.length)

def get_width(product):
    if ( (product.width == None) and (product.has_variants) ):
        return str(product.productvariation.parent.product.width)
    return str(product.width)

def get_height(product):
    if ( (product.height == None) and (product.has_variants) ):
        return str(product.productvariation.parent.product.height)
    return str(product.height)

def get_type(product):
    subtypes = product.get_subtypes()
    if len(subtypes) == 0:
        return ""
    else:
        return subtypes[0]

def get_price(product):
    return str(product.unit_price)

def get_slug(product):
    return str(product.slug)

    #if (product.has_variants):
    #    return "CHILD"
    #else:
    #    return "PARENT"

def get_parent(product):
    if (product.has_variants):
        return str(product.productvariation.parent.product.id)
    else:
        return ""

products = Product.objects.all().order_by('name')

sep = ","

for product in products:
    print (
        get_name(product) + sep
        + sep # for the link
        + get_id(product) + sep
        + get_weight(product) + sep
        + get_length(product) + sep
        + get_width(product) + sep
        + get_height(product) + sep
        + get_price(product) + sep
        + get_type(product) + sep
        + get_parent(product) + sep
        + get_slug(product) + sep
        + get_link(product) + sep
        )

