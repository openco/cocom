from django.core.mail import mail_admins
from satchmo_store.shop.models import Product
from store.localsite.models import lowproductquantitysettings
from django.db.models import F


newlylowproducts = Product.objects.filter(lowproductquantitysettings__limit__gt=F('items_in_stock'), lowproductquantitysettings__last_qty__gte=F('lowproductquantitysettings__limit'))

productlist = ["You are running low on the following items:","",""]

for p in newlylowproducts:
    print "You just ran out of " + p.name
    productlist.append(p.name)

if len(productlist ) > 3 :
    print "sending mail"
    mail_admins("Low inventory alert from OpenCo CoCom Site", ''.join(productlist) )

for p in Product.objects.all():
    l = lowproductquantitysettings.objects.get(product=p)
    l.last_qty=p.items_in_stock
    l.save()


