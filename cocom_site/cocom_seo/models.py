from django.db import models

# To make Django CMS plugins
from cms.models.pluginmodel import CMSPlugin

class SEOModel(CMSPlugin):

    description = models.TextField(blank=True)
    keywords = models.TextField(blank=True)

    def __unicode__(self):
        return self.description

