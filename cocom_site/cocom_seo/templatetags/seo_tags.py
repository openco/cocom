# For registering template tags
from django import template

# regex for parsing the html from render()
import re

register = template.Library()

# Render the tag
def seo_description(context):

    if not 'request' in context:
        return

    request = context['request']

    seomodel = get_seomodel_from_placeholder(request)
   
    # Error check
    if not seomodel:
        return ""

    if seomodel.description:
        return seomodel.description
    else:
        return ""

def seo_keywords(context):

    if not 'request' in context:
        return

    request = context['request']

    seomodel = get_seomodel_from_placeholder(request)

    # Error check
    if not seomodel:
        return ""

    if seomodel.keywords:
        return seomodel.keywords
    else:
        return ""

def get_seomodel_from_placeholder(request):
    #
    # Template tags are supposed to fail
    # silently.  So a lot of error checking
    # is included here.
    #

    page = request.current_page

    # Error check 
    if not page:
        # We're not on a CMS page.
        return None

    placeholders = page.placeholders.filter(slot='seo')

    # Error check 
    if not placeholders:
        # We're somehow using a template without the placeholder
        return None

    plugins = placeholders[0].get_plugins()

    # Error check
    if not plugins:
        # There's no plugin in the placeholder
        return None

    plugin = plugins[0]

    # Error check
    if hasattr(plugin, 'seomodel'):
        return plugin.seomodel
    else:
        return None

# Register the tag
register.simple_tag(takes_context=True)(seo_description)
register.simple_tag(takes_context=True)(seo_keywords)

