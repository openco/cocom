# CMS Plugin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

# Local models
from models import SEOModel

class SEOPlugin(CMSPluginBase):
    model = SEOModel
    name = _("SEO Plugin")
    render_template = "seo.html"

    def render(self, context, instance, placeholder):
        context['seo'] = instance
        return context

plugin_pool.register_plugin(SEOPlugin)
