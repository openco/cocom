#from django.conf.urls.defaults import *  # changed for django 1.6
from django.conf.urls import patterns, url, include
from django.views.generic.base import RedirectView
from django.conf import settings
from satchmo_store.urls import basepatterns
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin
admin.autodiscover()

from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

#import reportengine
#reportengine.autodiscover()

# For redirects
#from django.views.generic.simple import redirect_to


# For django rest
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

'''
# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
'''
urlpatterns = []

try:
    from local_urls import *
except ImportError:
    print "no local_urls"

urlpatterns = patterns('',
    # dajaxice
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),

    # Language free versions of the satchmo urls (This might not be necessary.)
    url(r'^shop/', include('satchmo_store.shop.urls')),

    # TODO Fix Satchmo URLS
    # Catch the old satchmo urls and forward them to shop/
    # (in case people have bookmarked pages)
    url('^product/(?P<slug>[a-zA-Z0-9_-]+)/$', RedirectView.as_view(url= '/shop/product/%(slug)s/')),
    url('^category/(?P<slug>[a-zA-Z0-9_-]+)/$',RedirectView.as_view(url= '/shop/category/%(slug)s/')),
    url('^cart/(?P<slug>[a-zA-Z0-9_-]+)/$',    RedirectView.as_view(url= '/shop/cart/%(slug)s/')),

    url(r'^jsi18n/(?P<packages>\S+?)/$', 'django.views.i18n.javascript_catalog'),
    #Form desiner form path for if a form is not on a cms page
    url(r'^forms/', include('form_designer.urls')),
    # TODO reportengine project doens't support django 1.6 and is discontinued.
    #url(r'^reports/', include('reportengine.urls')),
    # fix to stop having 404 due to missing favicon... renders black favicon

    # TODO Fix favicon view
    url(r'^favicon\.ico$', RedirectView.as_view(url=  '/static/images/favicon.ico')),
) + urlpatterns

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = patterns('',
    # redirects to static media files (css, javascript, images, etc.)
    url(r'^__debug__/', include(debug_toolbar.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns

urlpatterns += basepatterns + i18n_patterns('',
    # TODO Fix Satchmo URLS
    # Catch the old satchmo urls and forward them to shop/ (International Version)
    # (in case people have bookmarked pages)
    url('^product/(?P<slug>[a-zA-Z0-9_-]+)/$', RedirectView.as_view(url= '/shop/product/%(slug)s/')),
    url('^category/(?P<slug>[a-zA-Z0-9_-]+)/$',RedirectView.as_view(url= '/shop/category/%(slug)s/')),
    url('^cart/(?P<slug>[a-zA-Z0-9_-]+)/$',    RedirectView.as_view(url= '/shop/cart/%(slug)s/')),

    # admin pages
    url(r'^admin/', include(admin.site.urls)),
    # The language switcher sometimes prepends "p/" to the url. This catches these. Why does it do that??? 
    url(r'^p/', include('satchmo_store.shop.urls')),
    # Language enabled versions of the satchmo urls. 
    url(r'^shop/', include('satchmo_store.shop.urls')),
    # Language enabled versions of the django cms urls.
    url(r'^', include('cms.urls')),
    )

urlpatterns += staticfiles_urlpatterns()
