# Django settings for cocom_site project
from __future__ import unicode_literals
import logging, copy
from django.utils.log import DEFAULT_LOGGING
import os

LOGGING = copy.deepcopy(DEFAULT_LOGGING)
LOGGING["filters"]["suppress_deprecated"] = {
    "()": "cocom_site.settings.SuppressDeprecated"
}
LOGGING["handlers"]["console"]["filters"].append("suppress_deprecated")


class SuppressDeprecated(logging.Filter):

    def filter(self, record):
        WARNINGS_TO_SUPPRESS = ["RemovedInDjango18Warning", "RemovedInDjango19Warning"]
        # Return false to suppress message.
        return not any([warn in record.getMessage() for warn in WARNINGS_TO_SUPPRESS])


import os

# DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': 'djangotest.sqlite',
#        'USER': '',              # Not used with sqlite3.
#        'PASSWORD': '',          # Not used with sqlite3.
#        'HOST': '',              # Not used with sqlite3.
#        'PORT': '',              # Not used with sqlite3.
#        # for sqlite write lock timeout
#        'OPTIONS': {
#            'timeout': 5,
#        }
#    }
# }

gettext = lambda s: s
PROJECT_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]
DIRNAME = os.path.dirname(os.path.abspath(__file__))
SATCHMO_DIRNAME = DIRNAME

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DJANGO_PROJECT = "cocom_site"
DJANGO_SETTINGS_MODULE = "cocom_site.settings"

ADMINS = (
    (
        "",
        "",
    ),  # tuple (name, email) - important for error reports sending, if DEBUG is disabled.
)

MANAGERS = ADMINS

# Local time zone for this installation. All choices can be found here:
# http://www.postgresql.org/docs/current/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
TIME_ZONE = "America/Chicago"

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.4/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = "America/Chicago"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en"

LANGUAGES = [("en", "English"), ("fr", "French")]

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

APPEND_SLASH = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, "media/")
MEDIA_URL = "/media/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_PATH, "static/")
STATIC_URL = "/static/"
ADMIN_MEDIA_PREFIX = "/static/admin/"


# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    # 'dajaxice.finders.DajaxiceFinder',
    "djangobower.finders.BowerFinder",
    "compressor.finders.CompressorFinder",
)

# Specifie path to components root (you need to use absolute path):
BOWER_COMPONENTS_ROOT = os.path.join(PROJECT_PATH, "local_components/")

# Where djancho compressor will store it's compressed files
# This should be symlinked to a dir in  the client's repo
COMPRESS_ROOT = os.path.join(PROJECT_PATH, "local_compressed/")
COMPRESS_ENABLED = True
# Make this unique, and don't share it with anybody.
SECRET_KEY = ")_h&amp;+bhb+pj=$2^9ts68wsno@j*rnl)3n$1i#(ph#w*^g3517m"

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
    "django.template.loaders.eggs.Loader",
)

MIDDLEWARE_CLASSES = (
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.middleware.doc.XViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "threaded_multihost.middleware.ThreadLocalMiddleware",
    "satchmo_store.shop.SSLMiddleware.SSLRedirect",
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    "cms.middleware.page.CurrentPageMiddleware",
    "cms.middleware.user.CurrentUserMiddleware",
    "cms.middleware.toolbar.ToolbarMiddleware",
    "cms.middleware.language.LanguageCookieMiddleware",
    # 'cmsplugin_redirect.middleware.ForceResponseMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    "sslify.middleware.SSLifyMiddleware",
)

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

ROOT_URLCONF = "cocom_site.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "cocom_site.wsgi.application"

TEMPLATE_DIRS = (
    # The docs say it should be absolute path: PROJECT_PATH is precisely one.
    # Life is wonderful!
    os.path.join(PROJECT_PATH, "local_templates"),
    os.path.join(PROJECT_PATH, "templates"),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    # 'satchmo_store.shop.context_processors.settings',    # ROUND2
    "django.contrib.messages.context_processors.messages",
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.i18n",
    "django.core.context_processors.request",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "cms.context_processors.cms_settings",
    "sekizai.context_processors.sekizai",
)

MIGRATION_MODULES = {
    "cms": "cms.migrations_django",
    "menus": "menus.migrations_django",
    # Add also the following modules if you're using these plugins:
    "djangocms_file": "djangocms_file.migrations_django",
    "djangocms_flash": "djangocms_flash.migrations_django",
    "djangocms_googlemap": "djangocms_googlemap.migrations_django",
    # 'djangocms_inherit': 'djangocms_inherit.migrations_django',
    # 'djangocms_link': 'djangocms_link.migrations_django',
    # 'djangocms_picture': 'djangocms_picture.migrations_django',
    "djangocms_snippet": "djangocms_snippet.migrations_django",
    # 'djangocms_teaser': 'djangocms_teaser.migrations_django',
    # 'djangocms_video': 'djangocms_video.migrations_django',
    # 'djangocms_text_ckeditor': 'djangocms_text_ckeditor.migrations_django',
    "djangocms_text_ckeditor": "djangocms_text_ckeditor.migrations",
}

AUTHENTICATION_BACKENDS = (
    "satchmo_store.accounts.email-auth.EmailBackend",
    "django.contrib.auth.backends.ModelBackend",
)

SATCHMO_SETTINGS = {
    "SHOP_BASE": "/shop",
    "MULTISHOP": False,
    # 'DOCUMENT_CONVERTER': 'shipping.views.HTMLDocument',
    # 'SHOP_URLS' : patterns('satchmo_store.shop.views',)
}

gettext_noop = lambda s: s

# Configure logging
LOGFILE = "satchmo.log"
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
    datefmt="%a, %d %b %Y %H:%M:%S",
    filename=os.path.join(DIRNAME, LOGFILE),
    filemode="w",
)

logging.getLogger("django.db.backends").setLevel(logging.INFO)
logging.getLogger("keyedcache").setLevel(logging.INFO)
logging.getLogger("l10n").setLevel(logging.INFO)
logging.getLogger("suds").setLevel(logging.INFO)
logging.info("Satchmo Started")

THUMBNAIL_PROCESSORS = (
    "easy_thumbnails.processors.colorspace",
    "easy_thumbnails.processors.autocrop",
    # 'easy_thumbnails.processors.scale_and_crop',
    "filer.thumbnail_processors.scale_and_crop_with_subject_location",
    "easy_thumbnails.processors.filters",
)

SUIT_CONFIG = {
    "ADMIN_NAME": "CoCom Admin",
    "MENU_EXCLUDE": (
        "app_plugins",
        "area",
        "custom",
        "configurable",
        "cmsplugin_filer_image",
        "cocom_product",
        "registration",
        # '',
    ),
    "MENU_ICONS": {
        "auth": "icon-lock",
        "client_event": "icon-calendar",
        "cms": "icon-pencil",
        "cmsplugin_filer_image": "icon-wrench",
        "cocom_product": "icon-barcode",
        "comments": "icon-comment",
        "contact": "icon-user",
        "filer": "icon-folder-open",
        "l10n": "icon-globe",
        "product": "icon-barcode",
        "registration": "icon-bookmark",
        "shop": "icon-shopping-cart",
        "area": "icon-globe",
    },
    # 'MENU': (
    #    'sites',
    #    'auth',
    #    ''
    #    ),
}


# Feed settings
from django_feedparser.settings import *
from cmsplugin_feedparser.settings import *

# In case local_settings does not create this variable,
# initialize an empty tuple here.
INSTALLED_APPS = ()
CMS_TEMPLATES = ()
# Load the local settings
from local_settings import *

INSTALLED_APPS = (
    "djangobower",
    # 'satchmo_store.shop',        # ROUND2
    "smart_load_tag",
    "django.contrib.comments",
    "django.contrib.sitemaps",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "compressor",
    "dajaxice",
    "dajax",
    # 'grappelli',
    # 'django_admin_bootstrapped',
    "suit",
    # 'cmsplugin_text_ng',
    # Uncomment the next line to enable the admin:
    "django.contrib.admin",
    # Uncomment the next line to enable admin documentation:
    "django.contrib.admindocs",
    # 'reportengine',
    # Django-CMS stuff
    "djangocms_text_ckeditor",
    "cms",
    "mptt",
    "menus",
    # 'south', Not compatable with >=1.7
    "sekizai",
    "form_designer",
    # 'form_designer.contrib.cms_plugins.form_designer_form',
    "djangocms_flash",
    "djangocms_googlemap",
    "djangocms_link",
    "djangocms_snippet",
    # 'cms.plugins.picture',
    # 'cms.plugins.teaser',
    # 'cms.plugins.text',
    # 'cms.plugins.video',
    # Don't really need Twitter cms stuff in the base installl
    # 'cms.plugins.twitter',
    # 'cmsplugin_redirect',
    "filer",
    "easy_thumbnails",
    # 'cmsplugin_filer_file',
    # 'cmsplugin_filer_folder',
    # 'cmsplugin_filer_image',
    # 'cmsplugin_filer_teaser',
    # 'cmsplugin_filer_video',
    # 'reversion',
    "registration",
    "sorl.thumbnail",
    "keyedcache",
    "livesettings",
    "l10n",
    # 'satchmo_utils.thumbnail',# ROUND2
    # 'satchmo_store.contact',  # ROUND2
    # 'tax',                    # ROUND2
    # 'tax.modules.no',         # ROUND2
    # 'tax.modules.area',       # ROUND2
    # 'tax.modules.percent',    # ROUND2
    # 'shipping',               # ROUND2
    # 'satchmo_store.contact.supplier',
    # 'shipping.modules.tiered',
    # 'satchmo_ext.newsletter',
    # 'satchmo_ext.recentlist',
    # 'product',                      # ROUND2
    # 'product.modules.configurable', # ROUND2
    # 'product.modules.custom',       # ROUND2
    # 'product.modules.downloadable',
    # 'product.modules.subscription',
    # 'satchmo_ext.product_feeds',
    # 'satchmo_ext.brand',
    # 'payment',                         # ROUND2
    # Testing is payment dummy is the problem
    # 'payment.modules.dummy',
    # 'payment.modules.purchaseorder',
    # 'payment.modules.giftcertificate', # ROUND2
    # 'payment.modules.paypal',          # ROUND2
    # 'satchmo_ext.wishlist',
    # 'satchmo_ext.upsell',
    # 'satchmo_ext.productratings',
    # 'satchmo_ext.satchmo_toolbar',
    # 'satchmo_utils',                  # ROUND2
    # 'shipping.modules.tieredquantity',
    # 'satchmo_ext.tieredpricing',
    # 'debug_toolbar',
    "app_plugins",
    # 'cocom_site.localsite',
    "cocom_toggle",
    "cocom_site",
    # 'cocom_product',             # ROUND2
    "rest_framework",
    "django_feedparser",
    "cmsplugin_feedparser",
    "scheduler",
) + INSTALLED_APPS

DEBUG_TOOLBAR_PATCH_SETTINGS = False

DEBUG_TOOLBAR_PANELS = (
    # 'debug_toolbar.panels.version.VersionDebugPanel',
    # 'debug_toolbar.panels.timer.TimerDebugPanel',
    # 'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    # 'debug_toolbar.panels.headers.HeaderDebugPanel',
    # 'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    "debug_toolbar.panels.template.TemplateDebugPanel",
    # 'debug_toolbar.panels.sql.SQLDebugPanel',
    # 'debug_toolbar.panels.signals.SignalDebugPanel',
    # 'debug_toolbar.panels.logger.LoggingPanel',
)


def show_toolbar(request):
    if request.user and request.user.username == "openco":
        return True
    return False


DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": "cocom_site.settings.show_toolbar",
    "INTERCEPT_REDIRECTS": False,
}

CMS_SEO_FEILDS = True

CMS_TEMPLATES = (
    ("template_1.html", "Template One"),
    ("template_2.html", "Template Two"),
) + CMS_TEMPLATES

##################
#
# REST settings
#
#################

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly"
    ]
}

##################
#
# Monkey Patches
#
#################

"""
from django.apps import AppConfig

import sorl.thumbnail

class sorlConfig(AppConfig):
    label = 'sorl_thumbnail'

def new_sorl_init(self):
    self.__author__ = "Mikko Hellsing"
    self.__license__ = "BSD"
    self.__version__ = '12.3.MP'
    self.__maintainer__ = "Alexander Somma"
    self.__email__ = "admin@openco.ca"
    self.__status__ = "Production/MonkeyPatched"

    self.default_app_config = 'sorlConfig'

    class NullHandler(logging.Handler):
        def emit(self, record):
            pass

    # Add a logging handler that does nothing to silence messages with no logger
    # configured
    logging.getLogger('sorl').addHandler(NullHandler())


sorl.thumbnail.__init__ = new_sorl_init
"""


"""
# Monkey patch the file folder plugin so that it will load into ckeditor.
# This is to get slideshows inside the text editor.
from cmsplugin_filer_folder.cms_plugins import FilerFolderPlugin
# Override the classes text_enable attribute.
FilerFolderPlugin.text_enabled = True
# Override the classes icon_src method.
def icon_src(self, instance):
    return STATIC_URL + "slideshow.png"
FilerFolderPlugin.icon_src = icon_src

# Monkey patch the form designer plugin so that the user
# chooses forms based on "name" instead of "title"
from form_designer.models import FormDefinition
def form_unicode(self):
    return self.name or self.title
FormDefinition.__unicode__ = form_unicode
"""
