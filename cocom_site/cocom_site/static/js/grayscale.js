//jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
        $("a.navbar-brand > img").addClass("top-brand-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
        $("a.navbar-brand > img").removeClass("top-brand-collapse");
    }
});

//Equal Heights function
$.fn.equalHeights = function(px) {
	$(this).each(function(){
		if (window.matchMedia("(min-width: 1200px)").matches){
			var currentTallest = 0;
			$(this).children().each(function(i){
					if ($(this).height() > currentTallest) { currentTallest = $(this).height(); }
			});
			if (!px && Number.prototype.pxToEm) currentTallest = currentTallest.pxToEm(); //use ems unless px is specified
			// for ie6, set height since min-height isn't supported
			if ($.browser.msie && $.browser.version == 6.0) { $(this).children().css({'height': currentTallest}); }
			$(this).children().css({'min-height': currentTallest});
		}
		else {
			$(this).children().css({'min-height':'0'});
		}
	});
	return this;
};

$(function() {

	if (location.href.indexOf('#New_Callback') != -1) {
		try{
			$('html,body').scrollTop($($('form[name="New_Callback"]')[1]).offset().top-100);

		}
		catch(err){
		}
	}
	$(window).resize(function() {
		$('#page-top > div.row.happy-customers > div > div:nth-child(2)').equalHeights();
		$('#page-top > div.row.happy-customers > div > div:nth-child(4)').equalHeights();
	});
	$('#page-top > div.row.happy-customers > div > div:nth-child(2)').equalHeights();
	$('#page-top > div.row.happy-customers > div > div:nth-child(4)').equalHeights();
// Django form  styling
$('form[name="New_Callback"]').not('#myModal form').wrap('<div class="col-sm-12 contact"></div>');
$('#myModal form').wrap('<div class="col-sm-12"></div>');
$('form[name="New_Callback"]').addClass('col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4');
$('form input:not(.btn):not([type="file"])').addClass('form-control');
$('form textarea').addClass('form-control');
$('form[name="New_Callback"] #id_Phone').inputmask({ mask: '999-999-9999'});
// form falidation
$('form[name="New_Callback"]').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            Name: {
                message: 'Your name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Your name is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 60,
                        message: 'Your name must be more than 6 and less than 60 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9 ]+$/,
                        message: 'Your name can only consist of alpha, numbers and space'
                    }
                }
            },
            Email: {
                validators: {
                    notEmpty: {
                        message: 'Your email is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'Your input is not a valid email address'
                    }
                }
            },

        }
    });

var bootstrapValidator = $('form[name="New_Callback"]').data('bootstrapValidator');
console.log(bootstrapValidator.isValid());

//Form on SVG quick fixes
$('form:last-of-type input[type="submit"]').prop('value', 'Book It');	
$('#page-top > form').wrap('<div class="contact"></div>');
//Wrap the fist word of the menu items in a span
$(".navbar li a").each(function(index, element){
var node = $(this).contents().filter(function () { return this.nodeType == 3 }).first(),
    text = node.text(),
    first = text.slice(0, text.indexOf(" "));
if (!node.length)
    return;
node[0].nodeValue = text.slice(first.length);
node.before('<span>' + first + '</span>');
});
//Phone fix
$("a.call18882906099").attr("href", "tel:18882906099");

$('.carousel').carousel({
    pause: "false"
});

$('#myCarousel').carousel({
    interval: 6000
});

/// Deals with modal on mobile issue
$('div.modal-body > div > div.col-md-5.col-md-offset-1 > img').addClass('img-responsive');
$('.modal').each(
    function(){
        $(this).insertBefore($('#shop-furniture'));
    });

// handles the carousel thumbnails
$('[id^=carousel-selector-]').click( function(){
  var id_selector = $(this).attr("id");
  var id = id_selector.substr(id_selector.length -1);
  id = parseInt(id);
  $('#CreativaCarousel').carousel(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $(this).addClass('selected');
});

// when the carousel slides, auto update
$('#CreativaCarousel').on('slid', function (e) {
  var id = $('.item.active').data('slide-number');
  id = parseInt(id);
  $('[id^=carousel-selector-]').removeClass('selected');
  $('[id^=carousel-selector-'+id+']').addClass('selected');
});

//responsive galery
    var GammaSettings = {
		// order is important!
		viewport : [ {
			width : 1200,
			columns : 3
		}, {
			width : 900,
			columns : 3
		}, {
			width : 500,
			columns : 3
		}, { 
			width : 320,
			columns : 2
		}, { 
			width : 0,
			columns : 2
		} ]
    };

    Gamma.init( GammaSettings );


				// Example how to add more items (just a dummy):
/*
				var page = 0,
					items = ['<li><div data-alt="img03" data-description="<h3>Creativa Library</h3>" data-max-width="1800" data-max-height="1350"><div data-src="../Inspired-Images-web/08.jpg" data-min-width="200"></div><noscript></div></li>']
				function fncallback() {

					$( '#loadmore' ).show().on( 'click', function() {

						++page;
						var newitems = items[page-1]
						if( page <= 1 ) {

							Gamma.add( $( newitems ) );

						}
						if( page === 1 ) {

							$( this ).remove();

						}

					} );

				}


   */

     /*
     * Replace all SVG, class="svg", images with inline SVG
     */
        $('img.svg').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image's ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image's classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
    //jQuery for page scrolling feature - requires jQuery Easing plugin
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

//Google Map Skin - Get more at http://snazzymaps.com/
var myOptions = {
    zoom: 15,
    center: new google.maps.LatLng(53.385873, -1.471471),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles: [{
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 17
        }]
    }, {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 20
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 17
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 29
        }, {
            "weight": 0.2
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 18
        }]
    }, {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 16
        }]
    }, {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 21
        }]
    }, {
        "elementType": "labels.text.stroke",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#000000"
        }, {
            "lightness": 16
        }]
    }, {
        "elementType": "labels.text.fill",
        "stylers": [{
            "saturation": 36
        }, {
            "color": "#000000"
        }, {
            "lightness": 40
        }]
    }, {
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 19
        }]
    }, {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 20
        }]
    }, {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#000000"
        }, {
            "lightness": 17
        }, {
            "weight": 1.2
        }]
    }]
};



//var map = new google.maps.Map(document.getElementById('map'), myOptions);
