from store.localsite import urlshorten as us
from django import template

register = template.Library()

@register.filter
def urlshorten( order ):
    return us.shortorderurl( order )

