from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse

from django.core import serializers
from satchmo_store.shop.models import OrderPayment

# For parsing the admin search 
from django.db.models import Q
from django.db.models import Sum
from satchmo_store.shop.models import Order
from satchmo_store.shop.models import Cart
#from helpdesk.models import Ticket
import datetime
#from tc_refund import refund_transaction

# The options for status
# Currently:
# - the print program marks printed as an order variable after printing
class StatusVar:
    temp = 'Temp'
    new = 'New'
    blocked = 'Blocked'
    inProcess = 'In Process'
    billed = 'Billed'
    shipped = 'Shipped'
    completed = 'Complete'
    cancelled = 'Cancelled'


class admin:
    base = '/admin/'
    order = 'shop/order'

def example(request):
    ctx = RequestContext(request, {})
    return render_to_response('localsite/example.html', context_instance=ctx)

def faq(request):
    ctx = RequestContext(request, {})
    return render_to_response('mypages/faq.html', context_instance=ctx)

def tos(request):
    ctx = RequestContext(request, {})
    return render_to_response('mypages/tos.html', context_instance=ctx)

def aboutus(request):
    ctx = RequestContext(request, {})
    return render_to_response('mypages/aboutus.html', context_instance=ctx)

def howwegiveback(request):
    ctx = RequestContext(request, {})
    return render_to_response('mypages/howwegiveback.html', context_instance=ctx)

#def refund(request, order_id):
#
#    # Check for superuser
#    if (not request.user.is_superuser):
#        return HttpResponse("Not Superuser. Aborting. <br/>\n")
#
#    superuser_message = "Superuser: " + str(request.user.is_superuser) + "<br/>\n"
#    order_message = "Refunding this order: " + str(order_id) + "<br/>\n"
#
#    # Get the payments
#    order = Order.objects.get(pk=order_id)
#    payments = order.payments.all()
#    # Get number of payments
#    num_payments = len(payments)
#
#    # Check if there are payments
#    if (num_payments == 0):
#        return HttpResponse("There is no payment to refund. Aborting.<br/>\n")
#
#    # Get the final payment
#    final_payment = payments[num_payments-1]
#
#    # Check for non credit card
#    if (final_payment.payment != "TRUSTCOMMERCE"):
#        return HttpResponse("Final payment is not a credit card transaction. Aborting.<br/>\n")
#
#    # Assert: the last payment is a trustcommerce payment.
#
#    # Check for already refunded.
#    if (final_payment.details == "refund"):
#        return HttpResponse("Final payment is already a refund. Aborting.<br/>\n")
#
#    # Assert: the last payment is not a refund
#
#    # Mapping the order number to the transaction ID
#    transaction_id = final_payment.transaction_id
#    trans_message = "Refunding this transaction: " + str(transaction_id) + "<br/>\n"
#
#    amount = final_payment.amount
#
#    # Call the TC refund lib
#    refund = refund_transaction(transaction_id)
#    refund_message = "Trust Commerce: " + str(refund) + "<br/>"
#    refund_status = refund['status']
#
#    # Check if credit failed.
#    if (refund_status != "accepted"):
#        return HttpResponse( superuser_message + order_message + trans_message + refund_message)
#
#    # Assert: the refund was a success
#
#    # Get the transaction Id for the refund
#    refund_id = refund['transid']
#
#    # Crate a new payment object in the DB.
#    payments.create(
#        payment="TRUSTCOMMERCE", 
#        amount=(amount*-1), 
#        transaction_id=refund_id, 
#        details="refund", 
#        reason_code=refund_status, 
#        order_id=order_id)
#
#    # Set order status to canceled.
#    stat = order.orderstatus_set.create(status='Cancelled')
#    status_message = 'Status: ' + str(stat)
#
#    # Return the messages
#    return HttpResponse( superuser_message + order_message + trans_message + refund_message + status_message)

def adminSearch(request):
    return HttpResponse("Search for name or email")

def adminSearchResponse(request, search):
    orderCount = Order.objects.filter(
        Q(contact__last_name__icontains=search)|
        Q(contact__first_name__icontains=search)|
        Q(contact__email__icontains=search)
        ).count()
    cartCount = Cart.objects.filter(
        Q(customer__last_name__icontains=search)|
        Q(customer__first_name__icontains=search)|
        Q(customer__email__icontains=search)
        ).count()
    ticketCount = Ticket.objects.filter(submitter_email__icontains=search).count()
    return HttpResponse("Search for %s returned  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='https://secure.laughingmancheckout.com/helpdesk/tickets/?q=%s' target='_blank'>%s Tickets</a> <a href='https://secure.laughingmancheckout.com/admin/shop/cart/?q=%s'>%s Carts</a> <a href='https://secure.laughingmancheckout.com/admin/shop/order/?q=%s'>%s Orders</a> "
        %(search, search,ticketCount, search ,cartCount, search, orderCount))

def deltaSales(request):
    a_month_ago = (datetime.date.today() - datetime.timedelta(1*365/12))
    two_months_ago = (datetime.date.today() - datetime.timedelta(2*365/12))

    last_months_sales = Order.objects.filter(time_stamp__year=a_month_ago.year,time_stamp__month=a_month_ago.month).exclude(status__exact='').aggregate(Sum('total'))['total__sum']   
    the_other_months_sales = Order.objects.filter(time_stamp__year=two_months_ago.year,time_stamp__month=two_months_ago.month).exclude(status__exact='').aggregate(Sum('total'))['total__sum']
    last_months_sales_delta = ((last_months_sales - the_other_months_sales)/(the_other_months_sales))*100
    return HttpResponse("Sales: %.2f%%" % last_months_sales_delta)

def ticketCount(request):
    numberOfTickets = Ticket.objects.filter(Q(assigned_to__isnull=True)).count()
    if numberOfTickets > 0:
        response = ("<a href='/helpdesk/rss/unassigned/' style='color:red;'>%s Ticket(s)!</a>" %(numberOfTickets))
    else:
        response = ("")
    return HttpResponse(response)

def unshippedOrders(request):
    p = OrderPayment.objects.exclude(amount=0)
    p = p.order_by('time_stamp').reverse()
    p = p.exclude(order__status='Cancelled')
    p = p.exclude(order__status='Shipped')
    p = p.exclude(order__status='New')

    # Put all orders in a dictionary
    dictionary = {}
    for payment in p:
        dictionary[payment.order.pk] = payment

    # Get sorted orders ids
        keys = sorted(dictionary.keys(), reverse=True)

    # Create list from dictionary
    unique_list = []
    for key in keys:
        unique_list.append(dictionary[key]) 

    response = serializers.serialize('json', unique_list, fields=('order','time_stamp',))
    return HttpResponse(response, content_type="application/json")

def fullUnshippedOrders(request):
    p = OrderPayment.objects.exclude(amount=0)
    p = p.order_by('time_stamp').reverse()
    p = p.exclude(order__status='Cancelled')
    p = p.exclude(order__status='Shipped')
    p = p.exclude(order__status='New')

    # Put all orders in a dictionary
    dictionary = {}
    for payment in p:
        dictionary[payment.order.pk] = payment

    # Get sorted orders ids
        keys = sorted(dictionary.keys(), reverse=True)

    # Create list from dictionary
    unique_list = []
    for key in keys:
        unique_list.append(dictionary[key])  
    
    url = "/admin/shop/order/"
    r = [("<a id='order" + str(o.order.id)  + "' href='javascript://' onclick='shiporder(" + str(o.order.id)  + ")'>Ship: " + str(o.time_stamp) + " | " + o.order.contact.full_name + " " + "</a>") for o in unique_list[:10]]
    response = '\n'.join(r)
    return HttpResponse(response)


def unprintedOrders(request):
    p = OrderPayment.objects.exclude(amount=0)
    p = p.order_by('time_stamp').reverse()
    p = p.filter(order__status='In Process')
    p = p.filter(order__variables__key='Printing-Status', order__variables__value='Pending')
    o = [pay.order for pay in p]
    response = serializers.serialize('json', o, fields=('time_stamp',))
    return HttpResponse(response, content_type="application/json")

def set_order_variable(order, varKey, varValue):

    # Get the OrderVariable object
    vars = order.variables.filter(key=varKey)
    var = None
    if (len(vars) == 0):
        # If it doesn't exist, create one
        var = order.variables.create(key=varKey, value=varValue)
    else:
        # If it does exist, set it
        var = vars[0]
        var.value = varValue
        var.save()

def set_status(order, newStatus):
    stat = order.orderstatus_set.create(status=newStatus)

def orderShipped(request, order_id):
    order = Order.objects.get(pk=int(order_id))
    set_status(order, StatusVar.shipped)
    response = ("Order %s shipped" %(order_id))
    return HttpResponse(response)

def orderShippedByUSPS(request, tacking_number):
    p = OrderPayment.objects.exclude(amount=0)
    p = p.filter(order__status='In Process')
    p = p.filter(order__variables__key='Tracking-Number',order__variables__value=int(tracking_number))
    order = p[0].order
    response = ("Order %s shipped" %(order_id))
    return HttpResponse(response)

def orderPrinted(request, order_id):
    o = Order.objects.get(pk=int(order_id))
    varKey = 'Printing-Status'
    varValue = 'Printed'
    set_order_variable( o, varKey, varValue)
    response = ("Order %s Printed" %(order_id))
    return HttpResponse(response)
