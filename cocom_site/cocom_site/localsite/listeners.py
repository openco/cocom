import logging
log = logging.getLogger('satchmo_store.contact.forms')


def adjust_payment_choices(sender, contact, methods, **kwargs):
    log.warning('Running payment processor qualifier tests:')
    log.info('User:')
    log.info(contact)
    log.info('is a member of qualified users:')
    #log.info(bool(contact.user.groups.filter(name="Purchase Order Users")))

    if contact and contact.user and contact.user.groups:
        po_user = bool(contact.user.groups.filter(name="Purchase Order Users"))
    else:
        po_user = False

    if not po_user:
        log.info('Listing payment processors:')
        for method in methods:
            if method[0] == 'PURCHASEORDER':
                methods.remove(method)

def adjust_shipping_choices(order, shipping_options, shipping_dict,  **kwargs):
    manhatten_zips = [10026, 10027, 10030, 10037, 10039,            #Central Harlem
                      10001, 10011, 10018, 10019, 10020, 10036,     #Chelsea and Clinton
                      10029, 10035,                                 #East Harlem
                      10010, 10016, 10017, 10022,                   #Gramercy Park and Murray Hill
                      10012, 10013, 10014,                          #Greenwich Village and Soho
                      10004, 10005, 10006, 10007, 10038, 10280,     #Lower Manhattan
                      10002, 10003, 10009,                          #Lower East Side
                      10021, 10028, 10044, 10128,                   #Upper East Side
                      10023, 10024, 10025,                          #Upper West Side
                      10031, 10032, 10033, 10034, 10040]            #Inwood and Washington Heights
    log.warning ('Running shipping choice tests:')
    log.info(order)
    log.info(manhatten_zips)
    log.info(shipping_options)
    log.info(shipping_dict)
    if order.ship_postal_code:
        log.info(order.ship_postal_code[0:5])
        log.info(int(order.ship_postal_code[0:5]) in manhatten_zips)
        if not (int(order.ship_postal_code[0:5]) in manhatten_zips):
            for choice in shipping_options:
                if choice[0] == 'Messenger_Service':
                    shipping_options.remove(choice)
            shipping_dict.pop('Messenger_Service', None)


