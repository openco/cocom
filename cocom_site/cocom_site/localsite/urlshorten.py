from django.contrib.sites.models import Site
import bitly_api
from store.localsite.models import shorturlcache

def shortorderurl( order ):

    # Get the shorturl for this order from the cache.
    s = shorturlcache.objects.filter( order_id=order.pk )

    # If cache returns nothing, generate the shorturl
    if s.count() == 0:
        order_post_string = u''.join(['https://',
            Site.objects.get_current().domain,
            '/quickorder?quickpost=1'] )
        for item in order.orderitem_set.all():
            order_post_string = u''.join([
                order_post_string,
                u'&qty__',
                item.product.slug,
                u'=',
                str(item.quantity)]
            )

        a = bitly_api.Connection('livelaughingman', 'R_15a60039f7bc2568858f763429e1cf42')

        surl = a.shorten(order_post_string)

        uurl= u''.join( [surl['url'] ] )

        # Save the short urk to the cache
        s = shorturlcache( order_id = order.pk, url=uurl )
        s.save()
    else:
        s = s[0]


    return s.url
