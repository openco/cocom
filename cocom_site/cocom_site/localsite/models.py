from django.db import models
from satchmo_store.shop.models import Order, OrderItem, Product
from datetime import datetime
from django.db.models import Sum
from django.db.models import DateField, DecimalField, CharField, IntegerField, URLField, SlugField
from decimal import Decimal

from listeners import adjust_payment_choices, adjust_shipping_choices 
from shipping.signals import shipping_choices_query
from payment import signals as init

init.payment_methods_query.connect(adjust_payment_choices)
shipping_choices_query.connect(adjust_shipping_choices)

class dailysales(models.Model):
    date = DateField()

    def get_total_sales_today( self ):
        sales_today=Order.objects.filter(time_stamp__year=self.date.year,
                                time_stamp__month=self.date.month,
                                time_stamp__day=self.date.day).exclude(status__exact='').aggregate(
                                    Sum('total'))
        if sales_today['total__sum'] >0 :
            return sales_today['total__sum']
        else:
            return Decimal('0.00')

    #sales_total = property(_get_total_sales_today)
    #sales_total = DecimalField( max_digits = 100, decimal_places = 2, null=True)
    sales_total = DecimalField( max_digits = 65, decimal_places = 2, null=True)
    datestring = CharField( null = True, max_length = 20 )

class dailysalespercategory(models.Model):
    date = DateField()
    category = CharField( max_length = 512 )

    def get_total_sales_today( self ):
        sales_today=OrderItem.objects.filter(order__time_stamp__year=self.date.year,
                                order__time_stamp__month=self.date.month,
                                order__time_stamp__day=self.date.day)

        if self.category != u'All' and self.category!= '' and self.category != None:
            sales_today = sales_today.filter( product__category__name = self.category )

        sales_today_total = sales_today.aggregate( Sum('line_item_price') )
        discounts_today_total = sales_today.aggregate( Sum('discount') )

        if discounts_today_total['discount__sum'] > 0:
            discount = discounts_today_total['discount__sum']
        else:
            discount = Decimal("0.00")

        if sales_today_total['line_item_price__sum'] >0 :
            return sales_today_total['line_item_price__sum'] - discount
        else:
            return Decimal('0.00')

    #sales_total = property(_get_total_sales_today)
    #sales_total = DecimalField( max_digits = 100, decimal_places = 2, null=True)
    sales_total = DecimalField( max_digits = 65, decimal_places = 2, null=True)
    datestring = CharField( null = True, max_length = 20 )

class shorturlcache(models.Model):
    order_id = IntegerField()
    url = URLField()

class lowquantitysettings(models.Model):
    slug = SlugField( unique=True )
    limit = IntegerField()
    last_qty = IntegerField()


class lowproductquantitysettings(models.Model):
    product = models.ForeignKey( Product )
    limit = IntegerField()
    last_qty = IntegerField()
