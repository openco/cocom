from django.contrib import admin
from django.db import models
from product.models import Product
from product.admin import ProductOptions
from django import forms
from localsite.models import lowproductquantitysettings
from django.utils.translation import ugettext_lazy as _
"""
admin.site.unregister( Product )

class LQProductAdminForm( forms.ModelForm ):

    limit = forms.IntegerField(required=True)

    class Meta:
        model = Product


    def __init__( self, *args, **kwargs ):
        super( LQProductAdminForm, self ).__init__( *args, **kwargs )

        if kwargs.has_key('instance'):
            self.settings, _ = lowproductquantitysettings.objects.get_or_create( product=kwargs['instance'], defaults =
                { 'limit': 25, 'last_qty': 0 })
        else:
            self.settings = None
            return
        if not self.settings.limit:
            self.settings.limit = 0
            self.settings.save()

        self.initial['limit'] = self.settings.limit



    def save( self, commit = True ):
        model = super( LQProductAdminForm, self ).save( commit=False )
        if not self.settings:
            self.settings, _ = lowproductquantitysettings.objects.get_or_create( product=model, defaults =
                { 'limit': 25, 'last_qty': 0 })


        self.settings.limit = self.cleaned_data['limit']
        #for some reason, save() is never called with commit=True, so I have to save the limit value
        #even when commit is false. The rest of the fields still get saved, though.
        self.settings.save()

        if commit:
            model.save( commit = True )


        return model

class LQProductAdmin( ProductOptions ):
#class LQProductAdmin( admin.ModelAdmin ):
    form = LQProductAdminForm
    l = list(ProductOptions.fieldsets[0][1]['fields'])
    l.append('limit')
    ProductOptions.fieldsets[0][1]['fields'] = tuple( l )

#    fieldsets = (
#    (None, {'fields': ('site', 'category', 'name', 'slug', 'sku', 'description', 'short_description', 'date_added',
#            'active', 'featured', 'items_in_stock','total_sold','ordering', 'shipclass', 'limit')}),
#            (_('Meta Data'), {'fields': ('meta',), 'classes': ('collapse',)}),
#            (_('Item Dimensions'), {'fields': (('length', 'length_units','width','width_units','height','height_units'),('weight','weight_units')), 'classes': ('collapse',)}),
#            (_('Tax'), {'fields':('taxable', 'taxClass'), 'classes': ('collapse',)}),
#            (_('Related Products'), {'fields':('related_items','also_purchased'),'classes':('collapse',)}), )


admin.site.register(Product, LQProductAdmin)
"""
