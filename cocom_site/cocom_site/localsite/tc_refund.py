import tclink

'''
===============================================================================
This is example code from the documentation.

X. Postauths, Credits, and Chargebacks

   These three transaction types are similar in that they reference a
   successful transaction previously executed through the TrustCommerce
   system. These transaction types will not accept credit card information or
   other personal information fields. There is only one required parameter.

                              Required Parameters

+------------------------------------------------------------------------------+
| Parameter Name |                         Description                         |
|----------------+-------------------------------------------------------------|
| transid        | The transaction ID (format: nnn-nnnnnnnnnn) of the          |
|                | referenced transaction.                                     |
+------------------------------------------------------------------------------+

   In addition, postauths and credits can take an optional amount parameter.

                              Optional Parameters

+------------------------------------------------------------------------------+
| Parameter Name |                         Description                         |
|----------------+-------------------------------------------------------------|
| amount         | The amount, in cents, to credit or postauth if it is not    |
|                | the same as the original amount.                            |
+------------------------------------------------------------------------------+

   If no amount is passed, the default will be to take the amount from the
   original transaction. If, however, you wish to credit or postauthorize
   less than the original amount, you may pass this parameter.

  Credit Example

          params = {
                  'custid':    'mycustid',
                  'password':  'mypass',
                  'action':    'credit',
                  'transid':   '001-0000111101'
          }

          tclink.send(params)

          if (result['status'] != 'accepted'):
                  print 'Credit unsuccessful.'
'''


def refund_transaction(transactionID):

    # Force failure
    #transactionID = "0";

    # Create the parameters
    params = {
        'custid':    '1022600',
        'password':  'Ap9Thrpr',
        'action':    'credit',
        'transid':   transactionID
    }

    # Send it to Trust Commerce
    result = tclink.send(params)

    # Check result
    if result['status'] == 'accepted':
        print 'The transaction was approved!'
    else:
        print 'There was an error.'

    # Fake succcess
    #result['status'] = 'accepted'
    #result['transid'] = '789-012'

    return result

# Test Program
#print 'Using TCLink version', tclink.getVersion()
#print refund_transaction('0')
