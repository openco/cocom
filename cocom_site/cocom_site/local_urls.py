# from django.conf.urls.defaults import *
from django.conf.urls import patterns, url, include

from django.conf import settings
from satchmo_store.urls import basepatterns
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework.permissions import IsAuthenticated

# For django rest
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = []

try:
    from local_urls import *
except ImportError:
    print "no local_urls"

urlpatterns = patterns('',
    #(r'^grappelli/', include('grappelli.urls')),

    # Django REST API stuff
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #Temp direct port of old localsite API
    (r'^adminsearch/$', 'cocom_site.localsite.views.adminSearch'),
    (r'^adminsearch/(?P<search>.+)/$', 'cocom_site.localsite.views.adminSearchResponse'),
    (r'^unshipped.json$', 'cocom_site.localsite.views.unshippedOrders'),
    (r'^fullunshipped.html$', 'cocom_site.localsite.views.fullUnshippedOrders'),
    (r'^unprinted.json$', 'cocom_site.localsite.views.unprintedOrders'),
    (r'^printed/(?P<order_id>.+)$',  'cocom_site.localsite.views.orderPrinted'),
    (r'^ship/(?P<order_id>.+)$',  'cocom_site.localsite.views.orderShipped'),
    (r'^tracking_shipped/(?P<tracking_number>.+)$',  'cocom_site.localsite.views.orderShippedByUSPS'),
    #unported ...
    #(r'^admincmd/(?P<command>.+)-(?P<attribute>\d+) (?P<subcommand>.+)-(?P<subattribute>\d+)/$', 'store.views.admincommand'),
    #(r'^ticketcount/$', 'cocom_site.localsite.views.ticketCount'),
    #(r'^refund/(?P<order_id>.+)/$', 'cocom_site.localsite.views.refund'),
)
