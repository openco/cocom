# This settings file is for the client-project specific settings.
# Please update all the settings in the MANDATORY CONFIG section when starting.
import os, os.path

LOCAL_DEV = True
DEBUG = True
TEMPLATE_DEBUG = DEBUG

if LOCAL_DEV:
    INTERNAL_IPS = ("127.0.0.1",)

PROJECT_PATH = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]


##########################################################################
# START MANDATORY CONFIG
##########################################################################

DATABASES = {
    "default": {
        # The last part of ENGINE is 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'ado_mssql'.
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(
            PROJECT_PATH, "simple.db"
        ),  # Or path to database file if using sqlite3
        # 'USER': '',             # Not used with sqlite3.
        # 'PASSWORD': '',         # Not used with sqlite3.
        "HOST": "",  # Set to empty string for localhost. Not used with sqlite3.
        "PORT": "",  # Set to empty string for default. Not used with sqlite3.
    }
}


SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# This backend is not intended for use in production - it is provided as a convenience that can be used during development.
EMAIL_BACKEND = "django.core.mail.backends.dummy.EmailBackend"
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

#### Email Settings ####
# These must be set
# EMAIL_HOST = 'host here'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = 'your user here'
# EMAIL_HOST_PASSWORD = 'your password'
# EMAIL_USE_TLS = True

# These are used when loading the test data
SITE_DOMAIN = "localhost"
SITE_NAME = "Simple Satchmo"

##########################################################################
# END MANDATORY CONFIG
##########################################################################


ACCOUNT_ACTIVATION_DAYS = 7

# not suitable for deployment, for testing only, for deployment strongly consider memcached.
#### Cache settings ####
CACHE_BACKEND = "locmem:///"
CACHE_TIMEOUT = 60 * 5
# CACHE_PREFIX = "Z"  # depricated
